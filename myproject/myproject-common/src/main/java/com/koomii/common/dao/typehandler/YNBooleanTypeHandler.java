package com.koomii.common.dao.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

/**
 * 将Y/N字符串转换成boolean
 * @author lzy
 *
 */
public class YNBooleanTypeHandler implements TypeHandler<Boolean> {

	@Override
	public void setParameter(PreparedStatement ps, int i, Boolean parameter,
			JdbcType jdbcType) throws SQLException {
        ps.setString(i, parameter == Boolean.TRUE ? "Y" : "N");
	}

	@Override
	public Boolean getResult(ResultSet rs, String columnName)throws SQLException {
		return rs.getString(columnName) == "Y" ?Boolean.TRUE:Boolean.FALSE;
	}

	@Override
	public Boolean getResult(ResultSet rs, int columnIndex) throws SQLException {
		return rs.getString(columnIndex) == "Y" ?Boolean.TRUE:Boolean.FALSE;
	}

	@Override
	public Boolean getResult(CallableStatement cs, int columnIndex)
			throws SQLException {
		return cs.getString(columnIndex) == "Y" ?Boolean.TRUE:Boolean.FALSE;
	}
	
}
