package com.koomii.common.file;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: Greey
 * Date: 15-1-8
 * Time: 下午3:17
 * To change this template use File | Settings | File Templates.
 */
public interface FileStore {
    /***
     * 保存文件
     * @param newName 文件新名称 不需要带扩展名
     * @param file 需要保存的文件
     * @param fileType  文件类型 参考 FileType
     * @return 返回文件存储路径
     */
    public String saveFile(File file,int fileType,String newName);

    /***
     * 删除文件
     * @param path 文件保存路径
     * @return
     */
    public boolean deleteFile(String path,int fileType);
}
