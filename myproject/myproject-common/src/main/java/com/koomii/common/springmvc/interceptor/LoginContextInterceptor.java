package com.koomii.common.springmvc.interceptor;

import com.koomii.common.web.LoginContext;
import com.koomii.common.web.cookie.CookieUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * 
 * port from .NewLoginContextInterceptor
 * 
 * @see //.NewLoginContextInterceptor
 * @see //LoginContextInterceptor
 * @author
 * 
 */
public class LoginContextInterceptor extends HandlerInterceptorAdapter {

    private final static Log log = LogFactory
            .getLog(LoginContextInterceptor.class);
    protected CookieUtils cookieUtils;

    protected String loginCookieKey = "_dxh_";
    /**
     * 判断session有效时间，单位：秒 1800 为 30 * 60 。30分钟
     */
    protected int sessionTimeout = 1800;
    /**
     * 写入cookie的时机
     */
    protected int rate = 2;

    /**
     * 
     * @param cookieValue
     * @return
     */
    protected LoginContext getLoginContext(String cookieValue) {
        return LoginContext.parse(cookieValue);
    }

    public void setCookieUtils(CookieUtils cookieUtils) {
        this.cookieUtils = cookieUtils;
    }

    /**
     * 
     * @param loginCookieKey
     */
    public void setLoginCookieKey(String loginCookieKey) {
        this.loginCookieKey = loginCookieKey;
    }

    public void setSessionTimeout(int sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    @Override
    public final boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler)
            throws ServletException,
            IOException {

        try {
            updateLogin(request, response);
        } catch (Exception e) {
            log.warn("updatelogin error!", e);
        }

        return true;
    }
    public void postHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
            throws Exception {
        if(modelAndView!=null){
            //处理请求中的值
            LoginContext context = LoginContext.getLoginContext();
            if(context!=null && context.isLogin()){
                log.info("login success .user pin="+context.getPin());
                modelAndView.addObject("loginContext",context);
                request.removeAttribute("loginContext");
            }
        }
    }

    public void afterCompletion(
            HttpServletRequest request, HttpServletResponse response,
            Object handler, Exception ex)
            throws Exception {
        LoginContext.remove();
    }

    protected void updateLogin(HttpServletRequest request,
            HttpServletResponse response) {
        // 判断是否配置了cookie的cookie名称
        if (loginCookieKey != null) {
            try {
                String value = cookieUtils.getCookieValue(request,loginCookieKey);
                if (StringUtils.isNotBlank(value)) {// 能取到值
                    LoginContext context = getLoginContext(value);
                    if (context != null) {// 又能解出来
                        long current = System.currentTimeMillis();
                        long created = context.getCreated();
                        long expires = context.getExpires();
                        long timeout = expires == 0 ? sessionTimeout * 1000
                                : expires - created;// 如果没有设置过期时间，则使用默认的
                        if (current - created < timeout) { // 如果没有过期
                            LoginContext.setLoginContext(context);
                            if ((current - created) * rate > timeout) {// 如果剩下的时间只有2/3，就需要重新派发cookie
                                log.info("session cookie[" + loginCookieKey + "] rewrite!");
                                // 写最后一次访问的cookie
                                context.setCreated(current);
                                if (expires != 0) {
                                    context.setTimeout(timeout);
                                }
                                cookieUtils.setCookie(response, loginCookieKey,context.toCookieValue());
                            }
                        } else {
                            log.info("session cookie[" + loginCookieKey + "] is valid!");
                            // 超时后，要清空
                            cookieUtils.invalidate(request, response);
                        }
                    } else {
                        log.info("session cookie[" + loginCookieKey + "] is error!");
                    }
                } else {
                    log.info("session cookie[" + loginCookieKey + "] is empty!");
                }
            } catch (Exception e) {
                log.error("login intercept error", e);
            }
        } else {
            log.info("session cookie key is empty!");
        }

    }

}
