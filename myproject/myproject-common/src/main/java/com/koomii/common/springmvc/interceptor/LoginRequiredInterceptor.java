package com.koomii.common.springmvc.interceptor;

import com.koomii.common.util.JsonUtil;
import com.koomii.common.web.LoginContext;
import com.koomii.common.web.security.WebSecurity;
import com.koomii.common.web.url.UrlMakers;
import com.koomii.common.web.url.UrlMaker.PathMaker;
import com.koomii.myproject.common.OpResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.net.MalformedURLException;

/**
 * port from SecurityInterceptor
 *
 * @see //struts.interceptor.SecurityInterceptor
 *      检查此url请求是否需要登录
 */
public class LoginRequiredInterceptor extends HandlerInterceptorAdapter {

    private final static Log log = LogFactory.getLog(LoginRequiredInterceptor.class);
    protected UrlMakers urlMakers;
    protected WebSecurity webSecurity;
    protected String homeModule = "homeModule";
    protected String loginUrl = "loginUrl";


    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws IOException,
            MalformedURLException {

        String path = request.getRequestURI();
        //获取登录信息
        LoginContext context = getLoginContext();
        if (webSecurity != null) {
            //检查是否在受保护范围
            if (webSecurity.isProtected(path)) {
                //没有登录
                if (context == null || !context.isLogin()) {
                	processUnLoginResponse(request, response);
                    return false;
                }
            }
        } else {
            log.error("webSecurity is null,login is necessary");
            if (context == null || !context.isLogin()) {
            	processUnLoginResponse(request, response);
                return false;
            }
        }
        return true;
    }
    
    private void processUnLoginResponse(HttpServletRequest request,HttpServletResponse response) throws MalformedURLException, IOException{
    	String accept = request.getHeader("Accept");
    	if (accept.contains("application/json") || accept.contains("application/jsonp")) {
    		response.setCharacterEncoding("utf-8");
    		response.setContentType("application/json");
    		OpResult result = new OpResult(OpResult.OP_FAILED, "您尚未登录，请登录后查看！");
    		response.getWriter().write(JsonUtil.g.toJson(result));
//    		response.setStatus(HttpStatus.SC_UNAUTHORIZED);//401没权限
		}else{
			response.sendRedirect(getLoginUrl(request));
		}
    }

    /**
     * 取出登录的信息
     *
     * @return
     */
    protected LoginContext getLoginContext() {
        return LoginContext.getLoginContext();
    }

    @SuppressWarnings("unchecked")
    protected String getLoginUrl(HttpServletRequest request)
            throws MalformedURLException {
//        log.debug("composeed RequestURI:"+request.getRequestURI());
        PathMaker currentUrlBuilder = urlMakers.get(homeModule).path(
                request.getRequestURI());
        currentUrlBuilder.put(request.getParameterMap());
        currentUrlBuilder.remove("msg");
        currentUrlBuilder.remove("returnUrl");
        
        PathMaker loginUrlBuilder = urlMakers.get(loginUrl).path(null);
        loginUrlBuilder.put("returnUrl", currentUrlBuilder.make());
        return loginUrlBuilder.make();
    }

    public void setUrlMakers(UrlMakers urlMakers) {
        this.urlMakers = urlMakers;
    }

    public void setHomeModule(String homeModule) {
        this.homeModule = homeModule;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    public void setWebSecurity(WebSecurity webSecurity) {
        this.webSecurity = webSecurity;
    }
}
