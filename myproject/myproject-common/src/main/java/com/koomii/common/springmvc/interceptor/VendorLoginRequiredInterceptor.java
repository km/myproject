package com.koomii.common.springmvc.interceptor;

import com.koomii.common.web.LoginContext;
import com.koomii.common.web.VendorLoginContext;
import com.koomii.common.web.security.WebSecurity;
import com.koomii.common.web.url.UrlMakers;
import com.koomii.common.web.url.UrlMaker.PathMaker;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.net.MalformedURLException;

/**
 * port from SecurityInterceptor
 *
 * @author liuxiaofei
 * @see //struts.interceptor.SecurityInterceptor
 *      检查此url请求是否需要登录
 */
public class VendorLoginRequiredInterceptor extends HandlerInterceptorAdapter {

    private final static Log log = LogFactory.getLog(VendorLoginRequiredInterceptor.class);
    protected UrlMakers urlMakers;
    protected WebSecurity webSecurity;
    protected String homeModule = "homeModule";
    protected String loginUrl = "loginUrl";


    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws IOException,
            MalformedURLException {

        String path = request.getServletPath();
        //获取登录信息
        VendorLoginContext context = getVendorLoginContext();
        if (webSecurity != null) {
            //检查是否在受保护范围
            if (webSecurity.isProtected(path)) {
                //没有登录
                if (context == null || !context.isLogin()) {
                    response.sendRedirect(getLoginUrl(request));
                    return false;
                }
            }
        } else {
            log.error("migrSecurity is null,default login is necessary");
            if (context == null || !context.isLogin()) {
                response.sendRedirect(getLoginUrl(request));
                return false;
            }
        }
        return true;
    }

    /**
     * 取出登录的信息
     *
     * @return
     */
    protected VendorLoginContext getVendorLoginContext() {
        return VendorLoginContext.getVendorLoginContext();
    }

    @SuppressWarnings("unchecked")
    protected String getLoginUrl(HttpServletRequest request)
            throws MalformedURLException {
//        log.debug("composeed RequestURI:"+request.getRequestURI());
        PathMaker currentUrlBuilder = urlMakers.get(homeModule).path(
                request.getRequestURI());
        currentUrlBuilder.put(request.getParameterMap());
        PathMaker loginUrlBuilder = urlMakers.get(loginUrl).path(null);
        loginUrlBuilder.put("returnUrl", currentUrlBuilder.make());
        return loginUrlBuilder.make();
    }

    public void setUrlBuilders(UrlMakers urlMakers) {
        this.urlMakers = urlMakers;
    }

    public void setHomeModule(String homeModule) {
        this.homeModule = homeModule;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    public void setWebSecurity(WebSecurity webSecurity) {
        this.webSecurity = webSecurity;
    }
}
