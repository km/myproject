package com.koomii.common.tx;


import com.koomii.common.util.SequenceUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * tx基类
 * User: Administrator
 * Date: 2010-4-26
 * Time: 15:52:43
 */
public abstract class BaseTx {
	
	
    protected SequenceUtil sequenceUtil;

    public void setSequenceUtil(SequenceUtil sequenceUtil) {
        this.sequenceUtil = sequenceUtil;
    }

    @Autowired
    private PlatformTransactionManager transactionManager;

    public TransactionTemplate getDataSourceTransactionManager() {
        return new TransactionTemplate(transactionManager);
    }

    public void setTransactionManager(PlatformTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }
}
