package com.koomii.common.util;
/**
 * Describe:日志操作中字段修改信息
 * User:
 * Date: May 31, 2010
 * Time: 4:29:31 PM
 */
public class LogFieldInfo {
	private Long recordId;//记录id
	private String fieldName;//字段名
	private String changed;//是否有修改【值为Y,N】
	private String oldvalue;//原值
	private String newvalue;//改后值
	public String getChanged() {
		return changed;
	}
	public void setChanged(String changed) {
		this.changed = changed;
	}
	public String getOldvalue() {
		return oldvalue;
	}
	public void setOldvalue(String oldvalue) {
		this.oldvalue = oldvalue;
	}
	public String getNewvalue() {
		return newvalue;
	}
	public void setNewvalue(String newvalue) {
		this.newvalue = newvalue;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public Long getRecordId() {
		return recordId;
	}
	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
