package com.koomii.common.util;

import java.util.List;

/**
 * User: Administrator
 * Date: 2010-4-15
 * Time: 18:32:51
 */
public interface PaginatedList<T>{
	
	void add(T o);
	
	/**
	 * 设置数据结果集
	 */
	void setRows(List<T> dataList);
	
	/**
	 * 获取数据结果集
	 */
	List<T> getRows();

    /**
     * 是否为中间页
     *
     * @return
     */
    boolean isMiddlePage();


    /**
     * 是否为最后一页
     *
     * @return true 是, false 否
     */
    boolean isLastPage();

    /**
     * 是否还有下一页
     *
     * @return true 有, false 没有
     */
    boolean isNextPageAvailable();

    /**
     * 是否还有前一页
     *
     * @return true 是, false 否
     */
    boolean isPreviousPageAvailable();

    /**
     * 获取每页展示条数
     */
    int getPageSize();

    /**
     * 设置每页展示条数
     */
    void setPageSize(int pageSize);

    /**
     * 当前页页码
     */
    int getPage();

    /**
     * 当前页页码
     */
    void setPage(int index);

    /**
     * 总记录数
     */
    int getTotalItem();

    /**
     * 总记录数
     */
    void setTotalItem(int totalItem);

    /**
     * 总页数
     */
    int getTotalPage();

    /**
     * 开始记录数，从1开始
     */
    int getStartRow();

    /**
     * 结束记录数
     */
    int getEndRow();

    /**
     * 下一页页码
     */
    int getNextPage();

    /**
     * 前一页页码
     */
    int getPreviousPage();

    /**
     * 是否为第一页
     */
    boolean isFirstPage();
}
