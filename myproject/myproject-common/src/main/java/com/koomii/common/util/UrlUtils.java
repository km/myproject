package com.koomii.common.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;


public class UrlUtils {
	/**
	 * url加密
	 * @param value
	 * @return
	 */
	public static String urlEncode(String value){
		String result = "";
		if(StringUtils.isNotBlank(value)) {
	        try {
	            result = URLEncoder.encode(value, "utf-8");
	        } catch (UnsupportedEncodingException e) {
	            result = value;
	        }
		}
        return result;
	}
	
	/**
	 * url解密
	 * @param value
	 * @return
	 */
	public static String urlDecode(String value){
		String result = "";
		if(StringUtils.isNotBlank(value)) {
			try {
		    	result = URLDecoder.decode(value, "utf-8");
		   	} catch (UnsupportedEncodingException e) {
		    	result = value;
		  	}
		}
        return result;
	}
}
