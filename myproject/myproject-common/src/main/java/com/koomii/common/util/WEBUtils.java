package com.koomii.common.util;

import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class WEBUtils {

	/**
	 * cookie 域名
	 */
	public static final String COOKIE_DOMAIN ="3ajinrong.com";//"helloan.cn"
	
	/**
	 * cookie 存活时间
	 * 单位：秒
	 * 当前值：4小时
	 * 
	 */
	public static final int COOKIE_AGE =60*60*8;
	
	/**
	 * cookie 名称:to [user]
	 */
	public static final String USER_COOKIE_NAME="SmallCookie";
	
	public static final String SERVANT_COOKIE_NAME = "servantNO";
	
	/**
	 * 获取登录用户IP地址
	 * 
	 * @param request
	 * @return
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		if (ip.equals("0:0:0:0:0:0:0:1")) {
			ip = "本地";
		}
		return ip;
	}
	
	/**
	 * 得到服务器路径
	 * @param request
	 * @param matchURI
	 * @return
	 */
	public static String getBaseUrl(HttpServletRequest request, String matchURI) {
		StringBuffer requestURL = request.getRequestURL();
		return requestURL.substring(0, requestURL.lastIndexOf(matchURI));
	}
	
	/**
	 * 获取服务器路径(不包含项目路径)
	 * 比如：http://www.cloudsun.net/
	 * @param request
	 * @return
	 */
	public static String getServerUrl(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer(request.getScheme()).append("://");
		sb.append(request.getServerName()).append(":").append(request.getServerPort()).append("/");
		return sb.toString();
	}
	
	/**
	 * 获取服务器路径
	 * 例如
	 * http://bo.helloan.cn:80/hello3a-market/
	 * @param request
	 * @return
	 */
	public static String getBaseUrl(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer(request.getScheme()).append("://");
		sb.append(request.getServerName()).append(":").append(request.getServerPort()).append(request.getContextPath()).append("/");
		return sb.toString();
	}	
	
	/**
	 * 从spring中 获得request
	 * @return
	 */
	public static HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		return request;
	}
	
	
	
	/**
	 * 使用默认配置的 写cookie
	 * @param response
	 * @param key
	 * @param value
	 */
	public static void simpleWriteCookie(HttpServletResponse response,String key,String value){
		Cookie  cookie = new Cookie(key, value);
		cookie.setPath("/");
		cookie.setDomain(COOKIE_DOMAIN);
		cookie.setMaxAge(COOKIE_AGE);;
		response.addCookie(cookie);
	}
	/**
	 * 使用默认配置的 写cookie
	 * @param response
	 * @param key
	 * @param value
	 * @param expiry  过期时间 以秒为单位
	 */
	public static void simpleWriteCookie(HttpServletResponse response,String key,String value,int expiry){
		simpleWriteCookie(response, key, value);
	}
	
	/**
	 * 是否为 @Basic 所认同的 web 基础类型
	 */
	public static boolean isBasic(Class<?> clazz){
		if(clazz==null)return false;
		if(!clazz.isArray()){
			if(clazz.isPrimitive()||
					Date.class.isAssignableFrom(clazz)||
					clazz.equals(String.class)||
					Number.class.isAssignableFrom(clazz)||
					Character.class.equals(clazz)||
					Boolean.class.equals(clazz)){
				return true;
			}
		}else{
			Class<?> sub = clazz.getComponentType();
			if(Character.class.equals(sub)||
					byte.class.equals(sub)||
					Byte.class.equals(sub)||
					char.class.equals(sub)){
				return true;
			}
		}
		return false;
	}
}
