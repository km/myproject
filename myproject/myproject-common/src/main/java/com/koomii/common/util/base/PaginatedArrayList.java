package com.koomii.common.util.base;

import com.koomii.common.util.PaginatedList;

import java.util.ArrayList;
import java.util.List;

/**
 * ��ҳ��¼��ĸ���
 * User:
 * Date: 2010-4-15
 * Time: 18:34:44
 */
public class PaginatedArrayList<T> implements PaginatedList<T> {
    /**
     * 默认每页条数
     */
    public static final int PAGESIZE_DEFAULT = 20;
    /**
     * 每页条数
     */
    private int pageSize = PAGESIZE_DEFAULT;
    /**
     * 页码
     */
    private int page = 1;

    /**
     * 总记录数
     */
    private int totalItem = 0;
    /**
     * 总页数
     */
    private int totalPage = 0;

    /**
     * 开始条数
     */
    private int startRow = 1;
    /**
     * 结束条数
     */
    private int endRow = 1;
    
    /**
     * 结果集
     */
    private List<T> rows = new ArrayList<T>();

    /**
     * 初始化
     */
    public PaginatedArrayList() {
          repaginate();
    }

    /**
     * 初始化
     * @param page  当前页页码
     * @param pageSize 每页条数
     */
    public PaginatedArrayList(Integer page, Integer pageSize) {
    	if(page==null) {
    		page = 1;
		}
		if(pageSize==null) {
			pageSize = PAGESIZE_DEFAULT;
		}
        this.page = page;
        this.pageSize = pageSize;
        repaginate();
    }

    /**
     * 是否为第一页
     * @return true 是; false 否
     */
      public boolean isFirstPage(){
          return page <= 1;
      }


    public boolean isMiddlePage() {
         return !(isFirstPage() || isLastPage());
    }


    public boolean isLastPage() {
        return page >= totalPage;
    }


    public boolean isNextPageAvailable() {
        return !isLastPage();
    }

    public boolean isPreviousPageAvailable() {
          return !isFirstPage();
    }

    /**
     * 下一页的页码
     * @return
     */
    public int getNextPage() {
        if(isLastPage()) {
            return page;
        } else {
            return page+1;
        }
    }

    public int getPreviousPage() {
        if(isFirstPage()) {
            return 1;
        } else {
            return page - 1;
        }
    }
    /**
     * 每页条数
     */
    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
        repaginate();
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
        repaginate();
    }


    public int getTotalItem() {
        return totalItem;
    }

    public void setTotalItem(int totalItem) {
        this.totalItem = totalItem;
        repaginate();
    }

    public int getTotalPage() {
        return totalPage;
    }

    public int getStartRow() {
        return startRow;
    }
    
    public int getEndRow() {
        return endRow;
    }

    /**
     * Method repaginate ...
     */
    private void repaginate() {
        if (page < 1) {
            page = 1;
        }
        endRow = page * pageSize;
        startRow = endRow - pageSize + 1;
        if (totalItem > 0) {
            totalPage = totalItem / pageSize + (totalItem % pageSize > 0 ? 1 : 0);
            if(page > totalPage) {
                page = totalPage;
            }
            if(endRow>totalItem) {
                endRow = totalItem;
            }
        }
    }

	@Override
	public void setRows(List<T> dataList) {
		this.rows = dataList;
	}

	@Override
	public void add(T o) {
		rows.add(o);
	}

	@Override
	public List<T> getRows() {
		return rows;
	}
}
