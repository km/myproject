package com.koomii.common.util.security;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.code.kaptcha.Producer;
import com.google.code.kaptcha.util.Config;

public class CaptchaExtend {
	private Properties props = new Properties();
	private Config config = new Config(this.props);
	private Producer kaptchaProducer = null;
	private String sessionKeyValue = null;
	private String sessionKeyDateValue = null;

	public Properties getProps() {
		return props;
	}

	public void setProps(Properties props) {
		this.props = props;
		this.config = new Config(this.props);
		kaptchaProducer = kaptchaProducer();
		sessionKeyValue = sessionKeyValue();
		sessionKeyDateValue = sessionKeyDateValue();
	}

	public Config getConfig() {
		return config;
	}

	public void setConfig(Config config) {
		this.config = config;
	}

	private Producer kaptchaProducer() {
		return config.getProducerImpl();
	}

	private String sessionKeyValue() {
		return config.getSessionKey();
	}

	private String sessionKeyDateValue() {
		return config.getSessionDate();
	}

	public CaptchaExtend() {
		super();
	}

	public CaptchaExtend(Properties props) {
		// Switch off disk based caching.
		ImageIO.setUseCache(false);
		setProps(props);
		// this.props.put("kaptcha.border", "no");
		// this.props.put("kaptcha.textproducer.font.color", "black");
		// this.props.put("kaptcha.textproducer.char.space", "5");

	}

	/**
	 * map it to the /url/captcha.jpg
	 * 
	 * @param req
	 * @param resp
	 * @throws ServletException
	 * @throws IOException
	 */
	public void captcha(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// Set standard HTTP/1.1 no-cache headers.
		resp.setHeader("Cache-Control", "no-store, no-cache");

		// return a jpeg
		resp.setContentType("image/jpeg");

		// create the text for the image
		String capText = this.kaptchaProducer.createText();

		// store the text in the session
		req.getSession().setAttribute(this.sessionKeyValue(), capText);

		// store the date in the session so that it can be compared
		// against to make sure someone hasn't taken too long to enter
		// their kaptcha
		req.getSession().setAttribute(this.sessionKeyDateValue(), new Date());

		// create the image with the text
		BufferedImage bi = this.kaptchaProducer.createImage(capText);

		ServletOutputStream out = resp.getOutputStream();

		// write the data out
		ImageIO.write(bi, "jpg", out);
	}

	public String getGeneratedKey(HttpServletRequest req) {
		HttpSession session = req.getSession();
		return (String) session.getAttribute(this.sessionKeyValue);
	}
}
