package com.koomii.common.util.security;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import com.google.code.kaptcha.servlet.KaptchaExtend;
import com.google.code.kaptcha.util.Config;

/**
 * 
 * @author J.L
 *使用google 的 验证码 生成器
 */
public class CloudSunKaptcha extends KaptchaExtend{
	
	private static CloudSunKaptcha myself=null;
	

	public synchronized static CloudSunKaptcha getInstance(){
		if(myself==null){
			myself= new CloudSunKaptcha();
			
		}
		return myself;
	}
	
	
	
	 private Properties props = new Properties();

	    private Producer kaptchaProducer = null;

	    private String sessionKeyValue = null;

	    private String sessionKeyDateValue = null;

	    public CloudSunKaptcha() {
	        // Switch off disk based caching.
	        ImageIO.setUseCache(false);

	        this.props.put("kaptcha.border", "no");
	        //设置验证码字体颜色
	        this.props.put("kaptcha.textproducer.font.color", "black");
	        //设置字体间隔
	        this.props.put("kaptcha.textproducer.char.space", "5");
	        //设置验证码无干扰线
	        this.props.put("kaptcha.noise.impl","com.google.code.kaptcha.impl.NoNoise");
	        
	        //设置验证码图片中字符获取的实现类
	        this.props.put(Constants.KAPTCHA_TEXTPRODUCER_IMPL, CloudsunTextProducer.class.getName());

	        Config config = new Config(this.props);
	        this.kaptchaProducer = config.getProducerImpl();
	        this.sessionKeyValue = config.getSessionKey();
	        this.sessionKeyDateValue = config.getSessionDate();
	    }

	    /**
	     * map it to the /url/captcha.jpg
	     * 
	     * @param req
	     * @param resp
	     * @throws ServletException
	     * @throws IOException
	     */
	    public void captcha(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	        // Set standard HTTP/1.1 no-cache headers.
	        resp.setHeader("Cache-Control", "no-store, no-cache");

	        // return a jpeg
	        resp.setContentType("image/jpeg");

	        // create the text for the image
	        String capText = this.kaptchaProducer.createText();

	        // store the text in the session
	        req.getSession().setAttribute(this.sessionKeyValue, capText);

	        // store the date in the session so that it can be compared
	        // against to make sure someone hasn't taken too long to enter
	        // their kaptcha
	        req.getSession().setAttribute(this.sessionKeyDateValue, new Date());

	        // create the image with the text
	        BufferedImage bi = this.kaptchaProducer.createImage(capText);

	        ServletOutputStream out = resp.getOutputStream();

	        // write the data out
	        ImageIO.write(bi, "jpg", out);
	    }

	    public String getGeneratedKey(HttpServletRequest req) {
	        HttpSession session = req.getSession();
	        return (String) session.getAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);
	    }
}
