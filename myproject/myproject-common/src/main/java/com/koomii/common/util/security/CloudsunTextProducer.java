package com.koomii.common.util.security;

import java.util.Random;

import com.google.code.kaptcha.text.TextProducer;
/**
 * 验证码图片中的字符生成及范围类
 * @version 1.0
 * @since Sep 12, 2014 6:35:31 PM 
 * @update Sep 12, 2014 6:35:31 PM
 */
public class CloudsunTextProducer implements TextProducer{
	
	/**
	 * 字符范围
	 */
	private static final char[] HEHE = {'A','B','C','E','F','H','J','K','M','N','P','Q','R','T','U','V','W','X','Y'
		,'a','b','c','d','e','f','g','h','k','m','n','p','r','t','u','v','w','x','y'
		,'3','4','6','7','8','9'};
	
	/**
	 * 字符长度
	 */
	private static final int  LENGTH = 5;

	/**
	 * 获取验证码图片上所需字符
	 * @param random
	 * @return
	 * @version 1.0
	 * @update Sep 12, 2014 6:37:25 PM
	 */
	@Override
	public String getText() {
		Random random = new Random();
		StringBuilder sbBuilder = new StringBuilder();
		for(int i=0;i<LENGTH;i++){
			sbBuilder.append(getWord(random));
		}
		return sbBuilder.toString();
	}

	/**
	 * 随机生成字符范围中的一个字符
	 * @param random
	 * @return
	 * @version 1.0
	 * @update Sep 12, 2014 6:37:25 PM
	 */
	private String getWord(Random random){
		return HEHE[random.nextInt(HEHE.length)]+"";
	}
	
}
