package com.koomii.common.util.security;

import java.security.MessageDigest;

/**
 * 
 * @author J.L
 * 将明文 译为 密文
 */
public class EncryptionUtils {
	/**
	 * SHA1 加密算法
	 */
	public static final String  SHA1 ="sha1";
	/**
	 * MD5 加密算法
	 */
	public static final String  MD5 ="md5";
	
	/**
	 * SHA1 加密算法
	 */
	public  static String  bySHA1(String code){
		return encrypt(code,SHA1);
	}
	/**
	 * MD5 加密算法
	 */
	public  static String  byMD5(String code){
		return encrypt(code,MD5);
	}
	
	private  static  String  encrypt(String code,String algorithm){
		try{
			MessageDigest  md  = MessageDigest.getInstance(algorithm);
			byte[]  bytes = md.digest(code.getBytes("utf-8"));	
			BASE64Kit kit = new BASE64Kit();
			return kit.encode(bytes);
		}catch(Exception e){
			throw new IllegalArgumentException("加密算法异常！！cause:["+EncryptionUtils.class.getName()+"]");
		}
	}
	
}
