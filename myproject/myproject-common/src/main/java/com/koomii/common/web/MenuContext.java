package com.koomii.common.web;

import com.koomii.common.util.StringEscapeUtils;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Greey
 * Date: 14-11-26
 * Time: 下午12:05
 * 页面 菜单选中(class="active")实体类  放入cookies中
 */
public class MenuContext {
    private final static ThreadLocal<MenuContext> holder = new ThreadLocal<MenuContext>();
    private String one; //选中一级菜单编码
    private String two; //选中二级菜单编码
    private String three;//选中三级菜单编码
    private String userName;//登录用户名

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOne() {
        return one;
    }

    public void setOne(String one) {
        this.one = one;
    }

    public String getTwo() {
        return two;
    }

    public void setTwo(String two) {
        this.two = two;
    }

    public String getThree() {
        return three;
    }

    public void setThree(String three) {
        this.three = three;
    }
    public static void setMenuContext(MenuContext menuContext) {
        holder.set(menuContext);
    }

    /**
     * 取出登录的上下文
     *
     * @return null 如果没有的话
     */
    public static MenuContext getMenuContext() {
        return holder.get();
    }

    /**
     * 删除上下文、其实一般不用删除
     */
    public static void remove() {
        holder.remove();
    }
}
