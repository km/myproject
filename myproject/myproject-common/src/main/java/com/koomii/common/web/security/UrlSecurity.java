package com.koomii.common.web.security;

import java.util.ArrayList;
import java.util.List;

/**
 * 只有配置例外的url是保护的，其他都是不保护的
 * Time: 16:51:44
 */
public class UrlSecurity implements WebSecurity {
    private List<String> protectedPath;
    private List<String> protectedFile;

    public boolean isProtected(String path) {
        if (protectedFile != null && protectedFile.size() > 0) {
            for (String file : protectedFile) {
                if (file.equals(path)) {
                    return true;
                }
            }
        }
        if (protectedPath != null && protectedPath.size() > 0) {
            for (String file : protectedPath) {
                if (path.startsWith(file)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void setProtectedPath(List<String> protectedPath) {
        if (protectedPath != null) {
            List<String> newPath = new ArrayList<String>();
            for (String path : protectedPath) {
                path = path.replace('\\', '/');
                if (path.charAt(path.length() - 1) == '/') {
                    newPath.add(path);
                } else {
                    newPath.add(path + "/");
                }
            }
            this.protectedPath = newPath;
        }
    }

    public void setProtectedFile(List<String> protectedFile) {
        this.protectedFile = protectedFile;
    }
}
