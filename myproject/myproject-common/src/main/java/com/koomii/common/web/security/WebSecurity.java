package com.koomii.common.web.security;

/**
 *
 */
public interface WebSecurity {

    /**
     *
     *
     * @param path
     * @return
     */
    boolean isProtected(String path);
}