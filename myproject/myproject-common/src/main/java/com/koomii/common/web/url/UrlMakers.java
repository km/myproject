package com.koomii.common.web.url;

import java.util.Collections;
import java.util.Map;

/**
 * Safe predefined url builders container.
 * 
 * @author
 */
public class UrlMakers {
    private final Map<String, UrlMaker> urlMakers;

    public UrlMakers(Map<String, UrlMaker> urlMakers) {
        this.urlMakers = Collections.unmodifiableMap(urlMakers);
    }

    /**
     * fetch predefined url builder by key.
     * 
     * @param key
     * @return
     * @throws NullPointerException
     *             no url builder found.
     */
    public UrlMaker get(String key) throws NullPointerException {
        UrlMaker urlMaker = urlMakers.get(key);
        if (urlMaker == null) {
            throw new NullPointerException("No such predefined url builder: "
                    + key);
        }
        return urlMakers.get(key);
    }

}
