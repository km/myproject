package com.koomii.myproject.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Id生成器注解，类似JPA的@TableGenerator
 * @author lizongyue
 *
 */
@Target({java.lang.annotation.ElementType.METHOD, java.lang.annotation.ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IdGenerator {
	public abstract String idKey();
	public String pkPropertyName() default "id";
	public int allocationSize() default 100;
}
