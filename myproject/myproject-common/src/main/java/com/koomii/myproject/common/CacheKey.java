package com.koomii.myproject.common;

/**
 * Created with IntelliJ IDEA.
 * User: yfliuxiaofei
 * Date: 13-8-21
 * Time: 下午5:13
 * To change this template use File | Settings | File Templates.
 */
public enum CacheKey {
    /***
     * 缓存某用户拥有权限的的一级菜单集合
     * %s 替换值为 userName  若为空 表示缓存所有一级菜单
     * 时效 48小时
     */
    FIRST_MENU(SysConstants.SYS_CODE+"_FirstMenu_%s",60*60*48),
    /**
     * 子菜单缓存
     * 第一 %s 为userName   不为空的情况为缓存用户权限菜单
     * 第二 %s 为父级菜单编码
     */
    SUB_MENU(SysConstants.SYS_CODE+"_SubMenu_%s_%s",60*60*48),
    /**
     * 一级品类缓存
     */
    FIRST_CAT(SysConstants.SYS_CODE+"_FirstCat",60*60*48),
    /**
     * 商家系统参数缓存
     * 第一 %s vendorId
     * 第二 %s paramCode
     */
    Vendor_Param(SysConstants.SYS_CODE+"_VendorParam_%s_%s",60*60*48),
    /**
     * 列设置缓存
     * 第一个%s      为VendorId为      0时候为平台列设置
     * 第二个%s      为tableName
     */
    TABLE_DETAIL(SysConstants.SYS_CODE+"_TableDetial_%s_%s",60*60*48),
    /**
     * 列设置缓存
     * 第一个%s      为username
     */
    User_Role(SysConstants.SYS_CODE+"_UserRole_%s",60*60*48),
    /**
     * 列设置缓存
     * 第一个%s      为RoleID
     */
    Role_Menu(SysConstants.SYS_CODE+"_RoleMenu_%s",60*60*48),
    /**
     * 列设置缓存
     * 第一个%s      为username
     */
    User_Menu(SysConstants.SYS_CODE+"_UserMenu_%s",60*60*48),
    /**
     * 商家基础编码缓存
     * 第一个%s为vendorId   为0时候为平台列设置
     * 第二个%s为parentCode
     */
    Vendor_Basecode(SysConstants.SYS_CODE+"_VendorBasecode_%s_%s",60*60*48);

    private String key;
    private int term;

    private CacheKey(String key, int term) {
        this.key = key;
        this.term = term;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getTerm() {
        return term;
    }

    public void setTerm(int term) {
        this.term = term;
    }
}
