
package com.koomii.myproject.common;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: yfliuxiaofei
 * Date: 14-2-27
 * Time: 下午3:22
 * 前台操作提示类
 */
public class OpResult<E> implements Serializable {
    private static final long serialVersionUID = -5217263946675824590L;
    public static final int OP_SUCCESS = 1;
    public static final int OP_FAILED = 0;
    private int status;//1 成功  0失败
    private String errorCode;//错误业务码
    private String message;//提示信息
    private E dataValue;//数据值

    public OpResult(){}

    public OpResult(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getDataValue() {
        return dataValue;
    }

    public void setDataValue(E dataValue) {
        this.dataValue = dataValue;
    }
    public static final class OpMsg {
        public static final String SAVE_SUCCESS = "保存成功";
        public static final String SAVE_FAIL = "保存失败";
        public static final String MODIFY_SUCCESS = "修改成功";
        public static final String MODIFY_FAIL = "修改失败";
        public static final String DELETE_SUCCESS = "删除成功";
        public static final String DELETE_FAIL = "删除失败";
        public static final String OP_SUCCESS = "操作成功";
        public static final String OP_FAIL = "操作失败";
    }
    
    public static enum ErrorCode {
    	
    	Err_DB_001("数据库查询异常"),
    	Err_Bid_001("查询的标的不存在");
    	
        private String errorMsg;
        
        private ErrorCode(String errorMsg){
        	this.errorMsg = errorMsg;
        }

		public String getErrorMsg() {
			return errorMsg;
		}

    }
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}
