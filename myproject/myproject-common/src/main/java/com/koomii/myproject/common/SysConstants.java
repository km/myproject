package com.koomii.myproject.common;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * 系统常量类
 */
public class SysConstants {

    public static final Map<String, Map<String, String>> allConstAlias = new LinkedHashMap<String, Map<String, String>>();
    public static final String SYS_CODE="MS02";//dic字典表中子系统的编码
    /**
     * 初始化所有常量
     */
    static {
        try {
            for (Class<?> cls : SysConstants.class.getClasses()) {
                Map<String, String> constMap = new LinkedHashMap<String, String>();
                for (Field fd : cls.getDeclaredFields()) {
                    ConstAnnotation ca = fd.getAnnotation(ConstAnnotation.class);
                    if (ca != null) {
                        constMap.put(fd.get(cls).toString(), ca.value());
                    } else {
                        constMap.put(fd.get(cls).toString(), fd.getName());
                    }
                }
                allConstAlias.put(cls.getSimpleName(), constMap);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //delete flag 删除标志
    public static final class DF {
        @ConstAnnotation("已删除")
        public static final int DELETED = 1;
        @ConstAnnotation("正常")
        public static final int NORMAL = 0;
    }
    public static final class OPERATE {
        @ConstAnnotation("成功")
        public static final String SUCCESS = "00";
        @ConstAnnotation("失败")
        public static final String FAIL = "10";
    }
    public static final class Msg {
        public static final String NO_METHOD = "调用失败,m参数错误";
        public static final String SYS_ERROR = "调用失败";
        public static final String JSON_ERROR = "调用失败,json参数不能为空";
        public static final String PROCESS_ERROR = "调用失败,处理异常";
        public static final String PROCESS_SUCCESS = "操作成功";
        public static final String STOCK_NOT_ENOUGH = "库存不足";
        public static final String STOCK_EXCEPTION = "库存异常,不存在或已停售";
    }
    public static final class LoopLimit{
        public static final int YI_WAN = 10000;
        public static final int SHI_WAN = 100000;
        public static final int WUSHI_WAN = 500000;
    }
}
