package com.koomii.myproject.common;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Greey
 * Date: 14-12-4
 * Time: 下午2:37
 * To change this template use File | Settings | File Templates.
 */
public class TreeBean implements Serializable{
    private static final long serialVersionUID = -7966074105905852464L;
    private String id;//节点ID
    private String pId; //父节点ID
    private String name;//节点名称
    private String checked;//是否选中

    public TreeBean() {
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
