package com.koomii.myproject.config;

/**
 * 全局配置
 * @author david
 *
 */
public class GlobalConfig {
	/**
	 * 平台id
	 */
	public static final long SITE_ID = 22247;
	
	/**
	 * TOKEN过期时间分钟
	 */
	public static final int TOKEN_EXPILY_DATE=2;
	
	/**
	 * 总投资金额
	 */
	public static final String totalInvestMoney = "totalInvestMoney";
	/**
	 * 总投资收益
	 */
	public static final String totalInvestProfit = "totalInvestProfit";
	
    /** Resource标种的Modul */
	
	/* 标的线下协议 */
	public static final String RESOURCE_MODUL_BID_XIANXIA_YANG="net.bid.xianxia.yuan";
	public static final String RESOURCE_MODUL_BID_XIANXIA_SULU="net.bid.xianxia.sulu";
	
	/* 标的借款协议书 */
	public static final String RESOURCE_MODUL_BID_JIEKUAN_YANG="net.bid.jiekuan.yuan";
	public static final String RESOURCE_MODUL_BID_JIEKUAN_SULU="net.bid.jiekuan.sulu";
	
	/* 其他协议 */
	public static final String RESOURCE_MODUL_BID_QITA_YANG="net.bid.qita.yuan";
	public static final String RESOURCE_MODUL_BID_QITA_SULU="net.bid.qita.sulu";
	
	/* 借款人借款承诺书 */
	public static final String RESOURCE_MODUL_BORROW_CHNU_YANG="net.borrow.chnu.yuan";
	public static final String RESOURCE_MODUL_BORROW_CHNU_SULU="net.borrow.chnu.sulu";
	
	/* 借款人担保函*/
	public static final String RESOURCE_MODUL_BORROW_DABA_YANG="net.borrow.daba.yuan";
	public static final String RESOURCE_MODUL_BORROW_DABA_SULU="net.borrow.daba.sulu";
}
