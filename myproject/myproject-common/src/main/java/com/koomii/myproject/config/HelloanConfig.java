package com.koomii.myproject.config;

/**
 * @author J.L
 * 
 */
public class HelloanConfig {

	/**表 名*/
	public final static String TABLE_NAME_BIDSUBMITTION = "Helloan_BidSubmittion";
	/** 逾期还款，罚金分界线，天为单位。用于区分 轻度 和严重*/
	public final static int FINE_BOUNDARY =30;
	/** 商户登录后将用户类型设置为此类型*/
	public final static String USERTYPE_COMPANY = "user_type_company";
	/** 上传文件用户 类型商户 */
	public final static int UPLAOD_MERCHANT = 1;
	/** 上传文件用户 类型用户 */
	public final static int UPLAOD_USER = UPLAOD_MERCHANT + 1;
	/**理财帮助的文章类别ID*/
	public  final static int ARTICLECATEGORYID = 1;
	/**80分以下加入黑名单*/
	public static final int BLACK_SCORE = 80;
	/**90分以下补填资料*/
	public static final int APPEND_SCORE = 90;
	/**80 到90分一个担保人加的分数*/
	public static final int BONSMAN_SCORE = 10;
	/**允许的逾期90天以上次数*/
	public static final int MAX_OVER90COUNT = 1;
	/**允许的逾期的最大次数*/
	public static final int MAX_OVERCOUNT = 6;
	/**
	 *授信申请 最大的有效天数
	 */
	public static final int MAX_AUTHBORROW_ACTIVE_COUNT = 7;
	
	/**
	 * 免积分查看更多资料的积分临界值
	 * （应用于标的详情页）
	 */
	public static final int FORGIVE_INTEGRAL_LOOK_MOREINFO_CRITICAL = 1000;
}
