package com.koomii.myproject.config;

import java.util.HashMap;
import java.util.Map;

/**
 * 计算风险积分的关键字,总共46个定义的字段,再加上一个元字段
 * 
 * @author cherry 慕危
 * 
 */
public class ScoreItem {

    /** 风险计分项，“元字段” */
    public static final String SCORE_ITEM = "scoreItem";
    /** 年龄 */
    public static final String AGE = "AGE";
    /** 性别 */
    public static final String GENDER = "GENDER";
    /** 教育背景 */
    public static final String EDUCATION = "EDUCATION";
    /** 健康状况 */
    public static final String HEALTH = "HEALTH";
    /** 工作单位 */
    public static final String UNIT_PROPERTY = "UNIT_PROPERTY";
    /** 行业类别 */
    public static final String INDUSTRY = "INDUSTRY";
    /** 职位,职称 */
    public static final String DUTY = "DUTY";
    /** 职务 */
    public static final String POSITION = "POSITION";
    /** 月收入 */
    public static final String MONTHLY_INCOME = "MONTHLY_INCOME";
    /** 家庭年收入 */
    public static final String FAMILY_YEARLY_INCOME = "FAMILY_YEARLY_INCOME";
    /** 家庭每月积蓄 */
    public static final String FAMILY_MONTHLY_SAVE = "FAMILY_MONTHLY_SAVE";
    /** 家庭和睦 */
    public static final String DOMESTIC_PEACE = "DOMESTIC_PEACE";
    /** 住房情况 */
    public static final String HOUSE_CASE = "HOUSE_CASE";
    /** 投资情况 */
    public static final String INVEST_CASE = "INVEST_CASE";
    /** 大宗消费品 */
    public static final String BULK_GOODS = "BULK_GOODS";
    /** 家庭/企业资产 */
    public static final String HOME_ASSERTS = "HOME_ASSERTS";
    /** 家庭/企业固定资产 */
    public static final String HOME_FIXED_ASSERTS = "HOME_FIXED_ASSERTS";
    /** 已有负债 */
    public static final String DEBT = "DEBT";
    /** 债务收入比 */
    public static final String DEBT_INCOME_PERCENT = "DEBT_INCOME_PERCENT";
    /** 现单位工作年限 */
    public static final String WORK_YEARS = "WORK_YEARS";
    /** 在现址居住时间 */
    public static final String LIVING_TIME = "LIVING_TIME";
    /** 婚姻状况 */
    public static final String MARRIAGE = "MARRIAGE";
    /** 户口性质 */
    public static final String HOUSEHOLD_PROPERTY = "HOUSEHOLD_PROPERTY";
    /** 期限 */
    public static final String LOAN_EXPIRE = "LOAN_EXPIRE";
    /** 利率水平 */
    public static final String INTEREST_RATE = "INTEREST_RATE";
    /** 还款方式 */
    public static final String PAY_METHOD = "PAY_METHOD";
    /** 申请金额合理性 */
    public static final String LOAN_AMOUNT_RATIONAL = "LOAN_AMOUNT_RATIONAL";
    /** 申请金额 */
    public static final String LOAN_AMOUNT = "LOAN_AMOUNT";
    /** 银行贷款记录 */
    public static final String LOAN_RECORD = "LOAN_RECORD";
    /** 不良公共记录 */
    public static final String BAD_RECORD = "BAD_RECORD";
    /** 社会信誉,也就是社会知名度 */
    public static final String SOCIAL_STIGMA = "SOCIAL_STIGMA";
    /** 社会关系 */
    public static final String SOCIAL_RELATIONSHIP = "SOCIAL_RELATIONSHIP";
    /** 来源 */
    public static final String RESOURCE = "RESOURCE";
    /** 主观印象 */
    public static final String SUBJECTIVE_IMPRESSION = "SUBJECTIVE_IMPRESSION";
    /** 连带责任人 */
    public static final String JOINT_OWNER = "JOINT_OWNER";
    /** 连带责任人收入认证 */
    public static final String JOINT_OWNER_INCOME_QA = "JOINT_OWNER_INCOME_QA";
    /** 第三方保证 */
    public static final String THIRD_PARTY_QA = "THIRD_PARTH_QA";
    /*** 职业资格证 */
    public static final String JOB_QUALIFICATION = "JOB_QUALIFICATION";
    /** 联系人 */
    public static final String CONTACT = "CONTACT";
    /** 驾龄 */
    public static final String DRIVE_AGE = "DRIVE_AGE";
    /** 机动车认证 */
    public static final String CAR_CERTIFICATION = "CAR_CERTIFICATION";
    /** 第三方支付账户认证 */
    public static final String THIRD_PARTY_PAY_CERTIFICATION = "THIRD_PARTY_PAY_CERTIFICATION";
    /** 社交平台认证 */
    public static final String SOCIAL_PLATFORM_QA = "SOCIAL_PLATFORM_QA";
    /** QQ认证 */
    public static final String QQ = "QQ";
    /** 年里 在平台注册时间 */
    public static final String REGISTER_TIME = "REGISTER_TIME";
    /** 在平台业务往来 */
    public static final String BUSINESS_IN_HELLOAN = "BUSINESS_IN_HELLOAN";
    /** 在平台月交易量 */
    public static final String EXCHANGE_AMOUNT_IN_HELLOAN = "EXCHANGE_AMOUNT_IN_HELLOAN";
    /** 为了提高性能.增强安全性,将不再采用反射注解的形式,将中文和英文用键值對的形式存入map中 */
    private static Map<String, String> englishToChinese = new HashMap<String, String>(
	    46);

    // ==================================================
    // * 在静态代码块中,初始化这个Map
    // ==================================================
    static {

	/* 自然情况 4个 */
	englishToChinese.put(AGE, "年龄");// 1
	englishToChinese.put(GENDER, "性别");// 2
	englishToChinese.put(EDUCATION, "教育背景");// 3
	englishToChinese.put(HEALTH, "健康状况");// 4

	/* 职业状况 5个 */
	englishToChinese.put(UNIT_PROPERTY, "工作单位");// 5
	englishToChinese.put(INDUSTRY, "行业类别");// 6
	englishToChinese.put(DUTY, "职务");// 7
	englishToChinese.put(POSITION, "职位/职称");// 8
	englishToChinese.put(MONTHLY_INCOME, "月收入");// 9

	/* 家庭情况 3个 */
	englishToChinese.put(FAMILY_YEARLY_INCOME, "家庭年收入");// 10
	englishToChinese.put(FAMILY_MONTHLY_SAVE, "家庭每月积蓄");// 11
	englishToChinese.put(DOMESTIC_PEACE, "家庭和睦");// 12

	/* 保障支持 3个 */
	englishToChinese.put(HOUSE_CASE, "住房情况");// 13
	englishToChinese.put(INVEST_CASE, "投资情况");// 14
	englishToChinese.put(BULK_GOODS, "大宗消费品");// 15

	/* 经济支持 4个 */
	englishToChinese.put(HOME_ASSERTS, "家庭/企业资产");// 16
	englishToChinese.put(HOME_FIXED_ASSERTS, "家庭/企业固定资产");// 17
	englishToChinese.put(DEBT, "已有负债");// 18
	englishToChinese.put(DEBT_INCOME_PERCENT, "债务收入比");// 19

	/* 稳定情况 4个 */
	englishToChinese.put(WORK_YEARS, "现单位工作年限");// 20
	englishToChinese.put(LIVING_TIME, "在现地址居住时间");// 21
	englishToChinese.put(MARRIAGE, "婚姻状况");// 22
	englishToChinese.put(HOUSEHOLD_PROPERTY, "户口性质");// 23

	/* 贷款申请情况 5个 */
	englishToChinese.put(LOAN_EXPIRE, "贷款期限");// 24
	englishToChinese.put(INTEREST_RATE, "利率水平");// 25
	englishToChinese.put(PAY_METHOD, "还款方式");// 26
	englishToChinese.put(LOAN_AMOUNT_RATIONAL, "申请金额合理性");// 27
	englishToChinese.put(LOAN_AMOUNT, "申请金额");// 28

	/* 信用&其他 8个 */
	englishToChinese.put(LOAN_RECORD, "银行贷款记录");// 29
	englishToChinese.put(BAD_RECORD, "不良公共记录");// 30
	englishToChinese.put(SOCIAL_STIGMA, "社会信誉");// 31
	englishToChinese.put(SOCIAL_RELATIONSHIP, "社会关系");// 32
	englishToChinese.put(RESOURCE, "来源");// 33
	englishToChinese.put(SUBJECTIVE_IMPRESSION, "主观印象");// 34
	englishToChinese.put(JOINT_OWNER, "连带责任人");// 35
	englishToChinese.put(JOINT_OWNER_INCOME_QA, "连带责任人收入认证");// 36

	/* 附加项 10个 */
	englishToChinese.put(JOB_QUALIFICATION, "职业资格证");// 38
	englishToChinese.put(CONTACT, "联系人");// 39
	englishToChinese.put(DRIVE_AGE, "驾龄");// 40
	englishToChinese.put(CAR_CERTIFICATION, "机动车认证");// 41
	englishToChinese.put(THIRD_PARTY_PAY_CERTIFICATION, "第三方支付账户认证");// 42
	englishToChinese.put(SOCIAL_PLATFORM_QA, "社交平台认证");// 43
	englishToChinese.put(QQ, "QQ认证");// 44
	englishToChinese.put(REGISTER_TIME, "在平台注册时间");// 45
	englishToChinese.put(BUSINESS_IN_HELLOAN, "在平台业务往来");// 46
	englishToChinese.put(EXCHANGE_AMOUNT_IN_HELLOAN, "在平台月交易量");// 47
    }

    public static String get(Object key) {
	return englishToChinese.get(key);
    }

    /**
     * @return the englishToChinese
     */
    public static Map<String, String> getEnglishToChinese() {
	return englishToChinese;
    }

    /**
     * 不允许构造该类实例
     */
    private ScoreItem() {
    }
}
