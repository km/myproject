package com.koomii.myproject.enumeration;

/**
 * Biz状态码
 * @author david
 *
 */
public enum BizCode {
	
	LOGIN_SUCCESS("10001","登录成功"),
	LOGIN_TIMEOUT("10002","登录超时，请重新登录。"),
	LOGOUT_SUCCESS("10009","登出成功"),
	
	BID_NOT_EXIST("20001","投资项目不存在,请刷新后重新操作"),
	BID_INVALID_AMOUNT("20002","投资金额不合法"),
	BID_ACCOUNT_NOT_TRADECERT("20003","您尚未进行交易资格认证，请认证后再投标"),
	BID_IS_FULL("20004","此项目已满标，请选择其他项目投资"),
	BID_SUBMIT_FAILED("20005","投标失败，请刷新页面后重试"),
	BID_SUBMIT_SUCCESS("20006","投标成功"),
	
	ACCOUNT_NOT_TRADEAUTH("30001","尚未进行交易资格认证"),
	ACCOUNT_MONEY_LOW("30002","您的可用金额不足，请充值"),
	
	USER_NOTEXIST("40001","用户不存在"),
	
	VALIDATE_SIGNUP_HASUSERNAME("90101","用户名重复"),
	VALIDATE_SIGNUP_INVALIDCODE("90102","验证码错误"),
	
	SYS_ERROR_EXCEPTION("99999","系统异常")
	;
	private String code;
	private String text;
	
	private BizCode(String code,String text) {
		this.code = code;
		this.text = text;
	}
	public String getCode() {
		return code;
	}

	public String getText() {
		return text;
	}

}
