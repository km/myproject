package com.koomii.myproject.enumeration;

/**
 * 终端类型
 * @author david
 *
 */
public enum ClientType {
	android,ios,mac,win
}
