package com.koomii.myproject.exception;

public class BussinessException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	/*
	 * 异常信息
	 */
	private String message;
	/**
	 * 异常源
	 */
	private Throwable e;
	/**
	 * 异常码
	 */
	private String code;
	
	
	public BussinessException(String message){
		this.message = message;
	}
	
	public BussinessException(String message,Throwable e){
		this.message = message;
		this.e = e;
	}
	
	
	@Override
	public void printStackTrace() {
		e.printStackTrace();
		super.printStackTrace();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
