package com.koomii.myproject.exception;

public class NoSuchPhoneException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public NoSuchPhoneException() {
		super();
	}

	public NoSuchPhoneException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public NoSuchPhoneException(String userName) {
		super(userName);
	}

	public NoSuchPhoneException(Throwable arg0) {
		super(arg0);
	}

}

