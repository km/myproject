package com.koomii.myproject.exception;

/**
 * 乐观锁异常
 * @author lizongyue
 *
 */
public class StaleObjectStateException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	/*
	 * 异常信息
	 */
	private String message;
	/**
	 * 异常源
	 */
	private Throwable e;
	/**
	 * 异常码
	 */
	private String code;
	
	
	/**
	 * 乐观锁版本冲突异常
	 * @param message
	 */
	public StaleObjectStateException(String message){
		this.message = message;
	}
	
	public StaleObjectStateException(String message,Throwable e){
		this.message = message;
		this.e = e;
	}
	
	
	@Override
	public void printStackTrace() {
		e.printStackTrace();
		super.printStackTrace();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


}
