package com.koomii.myproject.mybatis.plugin;

import java.util.ArrayList;

/**
 * <dl>
 * <dt>PageList</dt>
 * <dd>Description:分页集合</dd>
 * </dl>
 * @author david
 */
public class PageList<E> extends ArrayList<E> {
	private static final long serialVersionUID = -3048177108454143126L;
	
    private int total; //总记录数
    
	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
}
