package com.koomii.myproject.sms;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 短信接口平台客户端
 * @version 1.0
 * @author 
 * @since 
 * @update 
 */
public class SMSClient {
//	private WebClient webclient = new WebClient();
	private static final String baseurl = " http://inolink.com/WS/BatchSend.aspx?";
	private static final Logger log = LoggerFactory.getLogger(SMSClient.class);
	
	
	private String corpID = "";// 序列号
	private String pwd = "";//密码
	
	public SMSClient(String username, String password){
		this.corpID =  username ;
		this.pwd = password;
	}
	
	public String mt(String mobile, String content, String ext, String stime,
			String rrid){
		try {
			return  sendSMS(corpID, pwd, mobile, content, stime);
			
//			return sendSms(username, password, mobile, content);
			//漫道短信
//			new SMSClient2("", "").mt(mobile, content, ext, stime, rrid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	
	
	/*
	 * 发送方法  其他方法同理      返回0 或者 1 都是  提交成功
	 * String ececcId 短信账号
	 * String password 密码
	 * String Mobile 发送手机号
	 * String Content 短信内容
	 * String send_time  定时发送时间
	 */
	private static String sendSMS(String ececcId, String password,
			String Mobile, String Content, String send_time)
			throws MalformedURLException, UnsupportedEncodingException {
		
		String send_content=URLEncoder.encode(Content.replaceAll("<br/>", " "), "GBK");//发送内容
		String urlStr = baseurl + "CorpID=" + ececcId + "&Pwd=" + password + "&Mobile="+Mobile+"&Content="+send_content+"&Cell=&SendTime="+send_time;
		URL url = new URL(urlStr);
		BufferedReader in = null;
		int inputLine = 0;
		try {
			log.info("发送短信手机号码 ："+Mobile);
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			inputLine = new Integer(in.readLine()).intValue();
		} catch (Exception e) {
			log.error("异常,发送短信失败！手机号码 【" + Mobile + "】"+ e);
			inputLine=-2;
		}
		log.info("结束发送短信返回值：  "+inputLine);
		return String.valueOf(inputLine);
	}
	
}
