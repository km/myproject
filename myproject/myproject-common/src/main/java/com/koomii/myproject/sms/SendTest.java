package com.koomii.myproject.sms;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;

import sun.misc.BASE64Encoder;

/**
 * 发送例子
 * 
 * @author liuyingying
 * 
 */
@SuppressWarnings("all")
public class SendTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String url = "http://pi.noc.cn/SendSMS.aspx";// utf-8编码时调用
		//第一种方法 通过resttemplete发送
//		RestTemplate template = new RestTemplate();
//		url +="?ececcid={ececcid}&password={password}&msisdn={msisdn}&smscontent={smscontent}&msgtype={msgtype}&longcode={longcode}";
//		Map<String,Object> root = new HashMap<String, Object>();
//		root.put("ececcid", "" + "305971017");
//		root.put("password", "heli");
//		root.put("msisdn", "13241660896");
//		root.put("smscontent", "test20140902");
//		root.put("msgtype", "" + "");
//		root.put("longcode", "");
//		String result =template.postForObject(url, null, String.class, root);
//		System.out.println(result);
		//1|1409021607034910|操作成功
		
		//-------------------------------------------------------------------------------
		//第二种方法 通过httpclient发送
		HttpClient client = new HttpClient();
		PostMethod postMethod = new UTF8PostMethod(url);
		// url = "http://127.0.0.1/SendSMSGB2312.aspx";//gb2312编码时调用
		// PostMethod postMethod = new GB2312PostMethod(url);
		postMethod.addParameter("ececcid", "" + "305971017");
		postMethod.addParameter("password", "heli");
		postMethod.addParameter("msisdn", "13241660896");
		postMethod.addParameter("smscontent", "test201409021628");
		postMethod.addParameter("msgtype", "" + "");
		postMethod.addParameter("longcode", "");
		try{
		int status = client.executeMethod(postMethod);
		if(status == 200){
			byte[] b = postMethod.getResponseBody();
			String string = new String(b);
			System.out.println(string);
			//1|1409021631196793|操作成功
		}else{
			System.out.println(status);
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//WebRoot/ReceiveMo.jps 为接收上行的例子， Report 也可以参考此例
		
		// TODO Auto-generated method stub
//		long ececcId = 0;
//		String password = "";
//		String nmsContentId = "";
//		String msisdn = "";
//		String subject = "";
		
		//上传F3资源
		//uploadNMSContent_Webservice(ececcId, password, "测试的%&= !@#$^&*()_-=`\'/?.,,");
		//uploadNMSContent_Http(ececcId, password,"测试的%&= !@#$^&*()_-=`\'/?.,,");
		//发送F3
		//sendNMS_Webservice(ececcId, password,msisdn,nmsContentId,subject);
		//sendNMS_Http(ececcId, password,msisdn,nmsContentId,subject);
		
		// 发送短信
		// sendSMS_Http(ececcId, password, msisdn,"测试的%&= !@#$^&*()_-=`\'/?.,,", 5, "");
		// sendSMS_Webservice(ececcId, password,msisdn,"测试的%&= !@#$^&*()_-=`\'/?.,,", "");

		// 发送彩信
		// sendMMS_Http(ececcId, password,msisdn,"测试的%&= !@#$^&*()_-=`\'/?.,,");
		// sendMMS_Webservice(ececcId, password, msisdn,"测试的%&= !@#$^&*()_-=`\'/?.,,");

	}

	/**
	 * 发送短信  企信通平台
	 * 
	 * @param ECECCID
	 *            账户
	 * @param password
	 *            密码
	 * @param msisdn
	 *            接收号码，多个用逗号隔开
	 * @param smsContent
	 *            短信内容
	 * @param longCode
	 *            扩展码
	 */
	public static void sendSMS_Http(long ececcId, String password,
			String msisdn, String smsContent, int msgType, String longCode) {

		HttpClient client = new HttpClient();
		String url = "http://pi.noc.cn/SendSMS.aspx";// utf-8编码时调用
		PostMethod postMethod = new UTF8PostMethod(url);

		// url = "http://127.0.0.1/SendSMSGB2312.aspx";//gb2312编码时调用
		// PostMethod postMethod = new GB2312PostMethod(url);

		postMethod.addParameter("ececcid", "" + ececcId);
		postMethod.addParameter("password", password);
		postMethod.addParameter("msisdn", msisdn);
		postMethod.addParameter("smscontent", smsContent);
		postMethod.addParameter("msgtype", "" + msgType);
		postMethod.addParameter("longcode", longCode);

		try {
			int status = client.executeMethod(postMethod);
			System.out.println(status);
			System.out
					.println(new String(postMethod.getResponseBody(), "UTF-8"));
			// System.out.println(new String(postMethod.getResponseBody(),
			// "GB2312"));

		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	

	/**
	 * 发送彩信
	 * 
	 * @param ECECCID
	 *            账户
	 * @param password
	 *            密码
	 * @param msisdn
	 *            接收号码，多个用逗号隔开
	 * @param subject
	 *            主题
	 */
	public static void sendMMS_Http(long ececcId, String password,
			String msisdn, String subject) {

		HttpClient client = new HttpClient();
		String url = "http://pi.f3.cn/SendMMS.aspx";
		PostMethod postMethod = new UTF8PostMethod(url);

		StringBuilder mmsContentsb = new StringBuilder();
		try {
			String txtContent = getBase64File("D:\\F3\\Data\\1.txt");
			if (txtContent.startsWith("77u/"))
				txtContent = txtContent.substring(4);

			// 第一帧
			mmsContentsb.append("1,jpg,").append(
					getBase64File("D:\\F3\\Data\\1.jpg")).append(";");
			mmsContentsb.append("1,txt,").append(txtContent).append(";");

			// 第二帧
			txtContent = new BASE64Encoder().encode(new String("你好第二帧"
					.getBytes("gb2312")).getBytes("utf-8"));
			mmsContentsb.append("2,txt,").append(txtContent).append(";");

			// 第三帧
			txtContent = new BASE64Encoder().encode(new String("你好第三帧"
					.getBytes("gb2312")).getBytes("utf-8"));
			mmsContentsb.append("3,txt,").append(txtContent).append(";");
		} catch (java.lang.Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		postMethod.addParameter("ececcid", "" + ececcId);
		postMethod.addParameter("password", password);
		postMethod.addParameter("msisdn", msisdn);
		postMethod.addParameter("subject", subject);
		postMethod.addParameter("mmscontent", mmsContentsb.toString());

		try {
			int status = client.executeMethod(postMethod);
			System.out.println(status);
			System.out
					.println(new String(postMethod.getResponseBody(), "utf-8"));

		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 上传F3资源
	 * @param ececcId
	 * @param password
	 */
	public static void uploadNMSContent_Http(long ececcId, String password, String subject) {
		HttpClient client = new HttpClient();
		String url = "http://pi.f3.cn/UploadNMSContent.aspx";// utf-8编码
		PostMethod postMethod = new UTF8PostMethod(url);
		String dir = System.getProperty("user.dir")
				+ "/TestSources/F3Content.zip";
		try {
			String nmcContent = getBase64File(dir);
			postMethod.addParameter("ececcid", "" + ececcId);
			postMethod.addParameter("password", password);
			postMethod.addParameter("subject", subject);
			postMethod.addParameter("nmsContent", nmcContent);
			postMethod.addParameter("scMsgType", "1");
			postMethod.addParameter("nmsVisitType", "0");// 0 公开 1 私有

			int status = client.executeMethod(postMethod);
			System.out.println(status);
			System.out
					.println(new String(postMethod.getResponseBody(), "UTF-8"));

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	/**
	 * 发送F3
	 * @param ececcId
	 * @param password
	 */
	public static void sendNMS_Http(long ececcId, String password, String msisdn, String nmsContentId, String subject) {
		HttpClient client = new HttpClient();
		String url = "http://pi.f3.cn/SendNMS.aspx";// utf-8编码
		PostMethod postMethod = new UTF8PostMethod(url);
		try {
			postMethod.addParameter("ececcid", "" + ececcId);
			postMethod.addParameter("password", password);
			postMethod.addParameter("tousertext", msisdn);
			postMethod.addParameter("nmsContentId", nmsContentId);
			postMethod.addParameter("subject", subject);
			int status = client.executeMethod(postMethod);
			System.out.println(status);
			System.out
					.println(new String(postMethod.getResponseBody(), "UTF-8"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	
	// Inner class for UTF-8 support
	public static class UTF8PostMethod extends PostMethod {
		public UTF8PostMethod(String url) {
			super(url);
		}

		@Override
		public String getRequestCharSet() {
			return "UTF-8";
		}
	}

	// Inner class for GB2312 support
	public static class GB2312PostMethod extends PostMethod {
		public GB2312PostMethod(String url) {
			super(url);
		}

		@Override
		public String getRequestCharSet() {
			return "GB2312";
		}
	}

	/**
	 * 如果读取的是文本文件，注意编码应为 UTF-8
	 * 
	 * @param imgurl
	 * @return
	 * @throws IOException
	 */
	public static String getBase64File(String filePath) throws IOException {
		String res = "";
		byte[] data = getFile(filePath);
		System.out.println(data.length);
		// 对字节数组Base64编码
		BASE64Encoder encoder = new BASE64Encoder();
		// 返回Base64编码过的字节数组字符串
		res = encoder.encode(data);
		// System.out.println(res);
		return res;
	}

	/**
	 * 如果读取的是文本文件，注意编码应为 UTF-8
	 * 
	 * @param imgurl
	 * @return
	 * @throws IOException
	 */
	public static byte[] getFile(String filePath) throws IOException {
		java.io.BufferedInputStream bis = null;
		try {

			File file = new File(filePath);
			InputStream in = new FileInputStream(file);
			bis = new BufferedInputStream(in);

			byte data[] = new byte[bis.available()];
			int i = 0;
			while ((i = bis.read(data)) != -1) {
				bis.read(data, 0, i);
			}
			return data;
		} catch (java.lang.Exception ex) {
			ex.printStackTrace();
		} finally {
			bis.close();
		}
		return null;
	}

	public void baseToChangeFile(String base64img) {
		try {
			byte[] result = new sun.misc.BASE64Decoder().decodeBuffer(base64img
					.trim());

			/*
			 * java.lang.IllegalArgumentException: Illegal mode "" must be one
			 * of "r", "rw", "rws", or "rwd" at
			 * java.io.RandomAccessFile.<init>(Unknown Source)
			 */
			// 注意：RandomAccessFile的第二参数必须是这四种的一个： r,rw,rws,rwd
			RandomAccessFile inOut = new RandomAccessFile("D:\\wenxin\\4.jpg",
					"rw");
			inOut.write(result);
			inOut.close();
		} catch (java.lang.Exception e) {
			e.printStackTrace();
		}
	}

}
