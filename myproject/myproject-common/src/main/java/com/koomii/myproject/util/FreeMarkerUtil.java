package com.koomii.myproject.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class FreeMarkerUtil {
	
	public static final String getClassText(String templateName,Object dataModel) throws TemplateException, IOException{
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		Configuration cfg = new Configuration();
		cfg.setTemplateLoader(new ClassTemplateLoader(FreeMarkerUtil.class, "/"));
		Template tm = cfg.getTemplate(templateName,"utf-8");
		tm.process(dataModel, pw);
		String text = sw.getBuffer().toString();
		IOUtils.closeQuietly(sw);
		IOUtils.closeQuietly(pw);
		return text;
	}

}
