package com.koomii.myproject.util;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

public class MapUtils {
	
	/**
	 * 获取 String
	 * @param m
	 * @param key
	 * @return
	 */
	public static String getString(Map<String, Object> m,String key) {
		String value = "";
		if(m!=null && m.containsKey(key)) {
			Object v = m.get(key);
			value = v!=null?v.toString():"";
		}
		return value;
	}
	
	/**
	 * 获取 Integer
	 * @param m
	 * @param key
	 * @return
	 */
	public static Integer getInteger(Map<String, Object> m,String key) {
		Integer value = 0;
		if(m!=null && m.containsKey(key)) {
			Object v = m.get(key);
			value = v!=null?Integer.parseInt(v.toString()):0;
		}
		return value;
	}
	
	/**
	 * 获取 BigDecimal
	 * @param m
	 * @param key
	 * @return
	 */
	public static BigDecimal getBigDecimal(Map<String, Object> m,String key) {
		BigDecimal value = BigDecimal.ZERO;
		if(m!=null && m.containsKey(key)) {
			Object v = m.get(key);
			value = v!=null?new BigDecimal(v.toString()):BigDecimal.ZERO;
		}
		return value;
	}
	/**
	 * 获取 BigDecimal
	 * @param m
	 * @param key
	 * @param scale
	 * @return
	 */
	public static BigDecimal getBigDecimal(Map<String, Object> m,String key,int scale) {
		BigDecimal value = BigDecimal.ZERO;
		if(m!=null && m.containsKey(key)) {
			Object v = m.get(key);
			value = v!=null?new BigDecimal(v.toString()):BigDecimal.ZERO;
			if(scale>=0) {
				value = value.divide(new BigDecimal(1),scale,BigDecimal.ROUND_HALF_UP);
			}
		}
		return value;
	}

	/**
	 * 获取 Date
	 * @param m
	 * @param key
	 * @return
	 */
	public static Date getDate(Map<String, Object> m,String key) {
		Date value = null;
		if(m!=null && m.containsKey(key)) {
			Object v = m.get(key);
			value = v!=null?new Date(((Timestamp)v).getTime()):null;
		}
		return value;
	}

}
