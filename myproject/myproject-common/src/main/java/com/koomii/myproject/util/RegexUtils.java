package com.koomii.myproject.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
 * 正则表达式工具类
 * @author david
 *
 */
public class RegexUtils {
	/**
     * 是否 是email 地址 使用了此正则表达�?^[0-9a-zA-Z\_\.]+@(([0-9a-zA-Z]+)[.])+[a-z]{2,4}$
     * 
     * @param str
     * @return
     */
    public static boolean isEmailAddress(String str) {
    	return matcher(str, "^[0-9a-zA-Z\\_\\.]+@(([0-9a-zA-Z]+)[.])+[a-z]{2,4}$");
    }
    /**
     * 验证是否是手机号  使用了此正则表达 ： ^[1][3,4,5,7,8][0-9]{9}$
     * @param str
     * @return
     */
    public static boolean isMobile(String str) {
    	return matcher(str, "^[1][3,4,5,7,8][0-9]{9}$");
    }
    
    /**
     * 验证是否是数字
     * @param str
     * @return
     */
    public static boolean isNumber(String str) {
    	return matcher(str, "^[0-9]*$");
    }

    /**
     * 验证是否金钱
     * @param str
     * @return
     */
    public static boolean isMoney(String str) {
        String regMoney="([0-9]+|[0-9]{1,3}(,[0-9]{3})*)(.[0-9]{1,2})?";
        return matcher(str,regMoney);
    }
    
    public static boolean isIdCard(String str) {
    	String regIdCard="(\\d{14}[0-9a-zA-Z])|(\\d{17}[0-9a-zA-Z])";
    	return matcher(str,regIdCard);
    }
    
    /**
     * 正则匹配
     * @param String -- 要验证的文字
     * @param test --- 正则表达式
     * @return
     */
    public static boolean matcher(String String, String test){
    	if (StringUtils.isBlank(String)) {
    	    return false;
    	}
    	Pattern pattern = Pattern.compile(test);
    		Matcher matcher = pattern.matcher(String);
    		return matcher.matches();
    }

    public static void main(String[] args) {
        System.out.println(isMoney("1.90"));
    }
}
