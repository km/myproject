package com.koomii.myproject.util;

public class StringUtils {

	/**
	 * 隐藏手机号
	 * @param mobile
	 * @return
	 */
	public static String hiddenMobile(String mobile) {
		String v = "";
		if(org.apache.commons.lang.StringUtils.isNotBlank(mobile) && mobile.length()>=11) {
			String start = mobile.substring(0, 3);
			String end = mobile.substring(mobile.length()-4, mobile.length());
			v = start + "****" + end;
		}
		return v;
	}
}
