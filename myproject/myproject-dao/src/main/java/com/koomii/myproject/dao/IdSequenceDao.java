package com.koomii.myproject.dao;


import org.apache.ibatis.annotations.Param;

import com.koomii.myproject.domain.entity.IdSequence;

/**
 *
 * IdSequenceDBDAO
 *
 * @author 
 *
 */
public interface IdSequenceDao{
	
	Long save(IdSequence idSequence);

	void update(IdSequence idSequence);

	IdSequence findByIdKeyForUpdate(@Param("idKey") String idKey);

}