package com.koomii.myproject.dao;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.koomii.myproject.domain.entity.Location;

/**
 *
 * LocationDBDAO
 *
 * @author 
 *
 */
public interface LocationDao{
	
	Long save(Location location);

	void update(@Param("location") Location location);

	void delete(@Param("id") Long id);

	Location findById(Integer id);
	/**
	 * 查询所有省份
	 * @return
	 */
	List<String> findProvinceList();
	/**
	 * 查询当前省份下的所有城市
	 * @return
	 */
	List<String> findCityList(@Param("province") String province);
	
	/**
	 * 跟据省 市 查询
	 * @param province
	 * @param city
	 * @return
	 */
	List<Location> findLocationList(@Param("province") String province,@Param("city") String city);
	
}