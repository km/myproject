package com.koomii.myproject.dao;


import org.apache.ibatis.annotations.Param;

import com.koomii.myproject.domain.entity.SmsMailLog;

/**
 *
 * SmsMailLogDBDAO
 *
 * @author 
 *
 */
public interface SmsMailLogDao{
	Integer save(SmsMailLog smsMailLog);

	void update(@Param("smsMailLog") SmsMailLog smsMailLog);

	void delete(@Param("id") Integer id);

	SmsMailLog findById(@Param("id") Integer id);
	
	/**
	 * 跟据手机号查询
	 * @param mobile
	 * @return
	 */
	SmsMailLog findByMobile(@Param("mobile") String mobile);

}