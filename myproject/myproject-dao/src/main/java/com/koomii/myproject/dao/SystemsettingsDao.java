package com.koomii.myproject.dao;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.koomii.myproject.annotation.IdGenerator;
import com.koomii.myproject.domain.entity.Systemsettings;

/**
 *
 * SystemsettingsDBDAO
 *
 * @author 
 *
 */
public interface SystemsettingsDao{
	@IdGenerator(idKey="net.cloudsun.common.entity.SystemSettings")
	Integer save(Systemsettings systemsettings);

	void update(Systemsettings systemsettings);

	void delete(@Param("id") Integer id);

	Systemsettings findById(@Param("id") Integer id);
	
	/**
	 * 查询配置
	 * @param paramName
	 * @param siteId
	 * @return
	 */
	List<Systemsettings> findByParam(@Param("paramName")String paramName,@Param("siteId")long siteId);

}