package com.koomii.myproject.domain.entity;


/**
 *
 *  mapper table : id_sequence
 *
 * @author zongyue.li
 *
 */
public class IdSequence{
	
	/**
	 * idKey
	 */
	private java.lang.String idKey;
	/**
	 * value
	 */
	private java.lang.Integer value;
	
	/**
	 * Get idKey
	 */
	public java.lang.String getIdKey() {
		return this.idKey;
	}
	
	/**
	 * Set idKey
	 */
	public void setIdKey(java.lang.String idKey) {
		this.idKey = idKey;
	}
	/**
	 * Get value
	 */
	public java.lang.Integer getValue() {
		return this.value;
	}
	
	public void increaseValue(){
		this.value += 1;
	}
	
	/**
	 * Set value
	 */
	public void setValue(java.lang.Integer value) {
		this.value = value;
	}
	
	public String toString() {
		return new StringBuffer().append("IdSequence{")
				.append("idKey='").append(getIdKey()).append("'")
				.append(",value='").append(getValue()).append("'")
				.append("}").toString();
		}
	}

