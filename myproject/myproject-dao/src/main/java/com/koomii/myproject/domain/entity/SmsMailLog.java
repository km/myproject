package com.koomii.myproject.domain.entity;


/**
 *
 *  mapper table : base_sms_mail_log
 *  短信和邮件日志
 * @author zongyue.li
 *
 */
public class SmsMailLog{
	
	private java.lang.Integer id;
	/**
	 * createDate
	 */
	private java.util.Date createDate;
	/**
	 * siteId
	 */
	private java.lang.Long siteId;
	/**
	 * version
	 */
	private java.lang.Integer version;
	/**
	 * 备注
	 * comment
	 */
	private java.lang.String comment;
	/**
	 * 发送结果
	 * sendResult
	 */
	private java.lang.String sendResult;
	/**
	 * 发送时间
	 * sendTime
	 */
	private java.util.Date sendTime;
	/**
	 * 发送者
	 * sendUserId
	 */
	private java.lang.Integer sendUserId;
	/**
	 * 用户类型，0标示系统，1 标示前台用户 ，2 表示 平台用户 3，标示商铺用户
	 * sendUserType
	 */
	private java.lang.Integer sendUserType;
	/**
	 * 发送状态
	 * status
	 */
	private java.lang.String status;
	/**
	 * 发送内容
	 * text
	 */
	private java.lang.String text;
	/**
	 * 短信手机号，邮箱
	 * title
	 */
	private java.lang.String title;
	/**
	 * 发送类型:短信SMS,邮箱Email
	 * type
	 */
	private java.lang.String type;
	/**
	 * 接受者用户
	 * userId
	 */
	private java.lang.Integer userId;
	/**
	 * 用户类型，1 标示前台用户 ，2 表示 平台用户 3，标示商铺用户
	 * userType
	 */
	private java.lang.Integer userType;
	
	/**
	 * Get id
	 */
	public java.lang.Integer getId() {
		return this.id;
	}
	
	/**
	 * Set id
	 */
	public void setId(java.lang.Integer id) {
		this.id = id;
	}
	/**
	 * Get createDate
	 */
	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	/**
	 * Set createDate
	 */
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * Get siteId
	 */
	public java.lang.Long getSiteId() {
		return this.siteId;
	}
	
	/**
	 * Set siteId
	 */
	public void setSiteId(java.lang.Long siteId) {
		this.siteId = siteId;
	}
	/**
	 * Get version
	 */
	public java.lang.Integer getVersion() {
		return this.version;
	}
	
	/**
	 * Set version
	 */
	public void setVersion(java.lang.Integer version) {
		this.version = version;
	}
	/**
	 * Get comment
	 */
	public java.lang.String getComment() {
		return this.comment;
	}
	
	/**
	 * Set comment
	 */
	public void setComment(java.lang.String comment) {
		this.comment = comment;
	}
	/**
	 * Get sendResult
	 */
	public java.lang.String getSendResult() {
		return this.sendResult;
	}
	
	/**
	 * Set sendResult
	 */
	public void setSendResult(java.lang.String sendResult) {
		this.sendResult = sendResult;
	}
	/**
	 * Get sendTime
	 */
	public java.util.Date getSendTime() {
		return this.sendTime;
	}
	
	/**
	 * Set sendTime
	 */
	public void setSendTime(java.util.Date sendTime) {
		this.sendTime = sendTime;
	}
	/**
	 * Get sendUserId
	 */
	public java.lang.Integer getSendUserId() {
		return this.sendUserId;
	}
	
	/**
	 * Set sendUserId
	 */
	public void setSendUserId(java.lang.Integer sendUserId) {
		this.sendUserId = sendUserId;
	}
	/**
	 * Get sendUserType
	 */
	public java.lang.Integer getSendUserType() {
		return this.sendUserType;
	}
	
	/**
	 * Set sendUserType
	 */
	public void setSendUserType(java.lang.Integer sendUserType) {
		this.sendUserType = sendUserType;
	}
	/**
	 * Get status
	 */
	public java.lang.String getStatus() {
		return this.status;
	}
	
	/**
	 * Set status
	 */
	public void setStatus(java.lang.String status) {
		this.status = status;
	}
	/**
	 * Get text
	 */
	public java.lang.String getText() {
		return this.text;
	}
	
	/**
	 * Set text
	 */
	public void setText(java.lang.String text) {
		this.text = text;
	}
	/**
	 * Get title
	 */
	public java.lang.String getTitle() {
		return this.title;
	}
	
	/**
	 * Set title
	 */
	public void setTitle(java.lang.String title) {
		this.title = title;
	}
	/**
	 * Get type
	 */
	public java.lang.String getType() {
		return this.type;
	}
	
	/**
	 * Set type
	 */
	public void setType(java.lang.String type) {
		this.type = type;
	}
	/**
	 * Get userId
	 */
	public java.lang.Integer getUserId() {
		return this.userId;
	}
	
	/**
	 * Set userId
	 */
	public void setUserId(java.lang.Integer userId) {
		this.userId = userId;
	}
	/**
	 * Get userType
	 */
	public java.lang.Integer getUserType() {
		return this.userType;
	}
	
	/**
	 * Set userType
	 */
	public void setUserType(java.lang.Integer userType) {
		this.userType = userType;
	}
	
	public String toString() {
		return new StringBuffer().append("SmsMailLog{")
				.append("id='").append(getId()).append("'")
				.append(",createDate='").append(getCreateDate()).append("'")
				.append(",siteId='").append(getSiteId()).append("'")
				.append(",version='").append(getVersion()).append("'")
				.append(",comment='").append(getComment()).append("'")
				.append(",sendResult='").append(getSendResult()).append("'")
				.append(",sendTime='").append(getSendTime()).append("'")
				.append(",sendUserId='").append(getSendUserId()).append("'")
				.append(",sendUserType='").append(getSendUserType()).append("'")
				.append(",status='").append(getStatus()).append("'")
				.append(",text='").append(getText()).append("'")
				.append(",title='").append(getTitle()).append("'")
				.append(",type='").append(getType()).append("'")
				.append(",userId='").append(getUserId()).append("'")
				.append(",userType='").append(getUserType()).append("'")
				.append("}").toString();
		}
	}

