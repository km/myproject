package com.koomii.myproject.domain.entity;


/**
 *
 *  mapper table : base_systemsettings
 *
 * @author zongyue.li
 *
 */
public class Systemsettings{
	
	private java.lang.Integer id;
	/**
	 * createDate
	 */
	private java.util.Date createDate;
	/**
	 * siteId
	 */
	private java.lang.Long siteId;
	/**
	 * version
	 */
	private java.lang.Integer version;
	/**
	 * category
	 */
	private java.lang.String category;
	/**
	 * paramName
	 */
	private java.lang.String paramName;
	/**
	 * paramValue
	 */
	private java.lang.String paramValue;
	/**
	 * relationship
	 */
	private java.lang.String relationship;
	
	/**
	 * Get id
	 */
	public java.lang.Integer getId() {
		return this.id;
	}
	
	/**
	 * Set id
	 */
	public void setId(java.lang.Integer id) {
		this.id = id;
	}
	/**
	 * Get createDate
	 */
	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	/**
	 * Set createDate
	 */
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * Get siteId
	 */
	public java.lang.Long getSiteId() {
		return this.siteId;
	}
	
	/**
	 * Set siteId
	 */
	public void setSiteId(java.lang.Long siteId) {
		this.siteId = siteId;
	}
	/**
	 * Get version
	 */
	public java.lang.Integer getVersion() {
		return this.version;
	}
	
	/**
	 * Set version
	 */
	public void setVersion(java.lang.Integer version) {
		this.version = version;
	}
	/**
	 * Get category
	 */
	public java.lang.String getCategory() {
		return this.category;
	}
	
	/**
	 * Set category
	 */
	public void setCategory(java.lang.String category) {
		this.category = category;
	}
	/**
	 * Get paramName
	 */
	public java.lang.String getParamName() {
		return this.paramName;
	}
	
	/**
	 * Set paramName
	 */
	public void setParamName(java.lang.String paramName) {
		this.paramName = paramName;
	}
	/**
	 * Get paramValue
	 */
	public java.lang.String getParamValue() {
		return this.paramValue;
	}
	
	/**
	 * Set paramValue
	 */
	public void setParamValue(java.lang.String paramValue) {
		this.paramValue = paramValue;
	}
	/**
	 * Get relationship
	 */
	public java.lang.String getRelationship() {
		return this.relationship;
	}
	
	/**
	 * Set relationship
	 */
	public void setRelationship(java.lang.String relationship) {
		this.relationship = relationship;
	}
	
	public String toString() {
		return new StringBuffer().append("Systemsettings{")
				.append("id='").append(getId()).append("'")
				.append(",createDate='").append(getCreateDate()).append("'")
				.append(",siteId='").append(getSiteId()).append("'")
				.append(",version='").append(getVersion()).append("'")
				.append(",category='").append(getCategory()).append("'")
				.append(",paramName='").append(getParamName()).append("'")
				.append(",paramValue='").append(getParamValue()).append("'")
				.append(",relationship='").append(getRelationship()).append("'")
				.append("}").toString();
		}
	}

