/**
 * 
 */
package com.koomii.myproject.domain.type;

/**
 * @author J.L  2014年5月27日
 *
 */
public enum Dollar {
	
	/**
	 * 充值手续费
	 */
	 TOP_UP_COST,
	
	/**
	 * 提现手续费
	 */
	WITHDRAW_DEPOSIT_COST,
	
	/**
	 * 平台服务费
	 */
	SITE_SERVICE_COST,
	
	/**
	 * 天标平台服务费
	 */
	DAY_SITE_SERVICE_COST,
	
	/**
	 * 风险准备金
	 */
	CASH_FOR_RISK,
	
	/**
	 * 借款管理费
	 */
	MANAGEMENT_COST,
	/**
	 * 天标借款管理费
	 */
	DAY_MANAGEMENT_COST,
	
	/**
	 * 投资服务费
	 */
	COST_OF_SERVE_INVESTMENT,
	
	/**
	 * 违约金
	 */
	LIQUIDATED_DAMAGES,
	
	/**
	 * 轻量逾期罚金 （利率）
	 */
	LIGHT_FINE,
	
	/**
	 * 严重逾期罚金（利率）
	 */
	HEAVY_FINE,
	
	
	/**
	 * 推荐人奖励（利率）
	 */
	AWARD_OF_INTRODUCE,
	
	/**
	 * 借款管理费分成比例（平台收）
	 */
	MANAGEMENT_COST_DIVIDE,
	
	/**
	 * 风险评估费分成比例(平台收)
	 */
	ASSESS_RISK_COST_DIVIDE,
	
	/**
	 * 风险评估费
	 */
	ASSESS_RISK_COST,
	
	/**
	 * 违约金分成比例(平台收)
	 */
	LIQUIDATED_DAMAGES_DIVIDE,
	
	/**
	 * 逾期罚金分成比例（平台收）
	 */
	FINE_DIVIDE;
}
