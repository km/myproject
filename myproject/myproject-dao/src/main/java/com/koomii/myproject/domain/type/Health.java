package com.koomii.myproject.domain.type;

public enum Health {
	BAD("近半年有过住院","差"), ORDINARY("近半年有费用超过500元生病但未住院","一般"), FINE("近半年无费用超过500元生病","好");
	
	private String text;
	private String norm;//标准
	

	Health(String text,String norm) {
		this.text = text;
		this.norm = norm;
	}

	public String getText() {
		return text;
	}

	public static Health get(int index) {
		for (Health d : values()) {
			if (d.ordinal() == index) {
				return d;
			}
		}
		return BAD;
	}

	public String getNorm() {
		return norm;
	}

}
