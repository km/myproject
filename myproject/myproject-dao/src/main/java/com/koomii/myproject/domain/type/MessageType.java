package com.koomii.myproject.domain.type;

/**
 * 发送信息 枚举
 *     SMS("手机短信")
 *     Email("电子邮件")
 * @author david
 *
 */
public enum MessageType {
	 /** 手机短信 */
    SMS("手机短信"),
    /** 标的认证 */
    Email("电子邮件");

    private String text;

    private MessageType(String text) {
	this.text = text;
    }

    public String getText() {
	return text;
    }
}
