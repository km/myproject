package com.koomii.myproject.domain.type;

public enum TradeID{
	
	Portal("3A金融"),Borrower("借款人"),Bid("标的"),Company("商户"),Investor("投资人");
	
	private String text;   
 
	private TradeID(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}
	
}
