package com.koomii.myproject.domain.type;

/**
 * 用户状态
 * 
 * @author 俞建波
 * 
 */
public enum UserStatus {

    ACTIVE("激活"), INACTIVE("非激活"), TRADEAUTH("完成交易认证");
    private String text;

    private UserStatus(String text) {
	this.text = text;
    }

    public String getText() {
	return text;
    }

    /**
     * 
     * @return 是否通过交易资格认证
     */
    public boolean isTradeAuth() {
    	if (this == TRADEAUTH) {
    		return true;
    	}
    	// 如果由后续状态，也符合交易资格认证，写在这后面
    	return false;
    }
}
