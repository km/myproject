package com.koomii.myproject.rpc.bean;


/**
 *
 *  mapper table : base_location
 *
 * @author zongyue.li
 *
 */
public class LocationBean{
	
	private java.lang.Integer id;
	/**
	 * createDate
	 */
	private java.util.Date createDate;
	/**
	 * siteId
	 */
	private java.lang.Long siteId;
	/**
	 * version
	 */
	private java.lang.Integer version;
	/**
	 * city
	 */
	private java.lang.String city;
	/**
	 * country
	 */
	private java.lang.String country;
	/**
	 * province
	 */
	private java.lang.String province;
	
	 /**
     * 
     * @return html页面代码（伪字段）
     */
    public String getHtml() {
		if(this.province== null){
		    return "";
		}
		if (this.province.equals("未设置")) {
		    return "未设置";
		}
		if (this.city.equals("市辖区")) {
		    return this.province + " " + this.country;
		} else {
		    return this.province + " " + this.city + " " + this.country;
		}
    }
	
	/**
	 * Get id
	 */
	public java.lang.Integer getId() {
		return this.id;
	}
	
	/**
	 * Set id
	 */
	public void setId(java.lang.Integer id) {
		this.id = id;
	}
	/**
	 * Get createDate
	 */
	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	/**
	 * Set createDate
	 */
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * Get siteId
	 */
	public java.lang.Long getSiteId() {
		return this.siteId;
	}
	
	/**
	 * Set siteId
	 */
	public void setSiteId(java.lang.Long siteId) {
		this.siteId = siteId;
	}
	/**
	 * Get version
	 */
	public java.lang.Integer getVersion() {
		return this.version;
	}
	
	/**
	 * Set version
	 */
	public void setVersion(java.lang.Integer version) {
		this.version = version;
	}
	/**
	 * Get city
	 */
	public java.lang.String getCity() {
		return this.city;
	}
	
	/**
	 * Set city
	 */
	public void setCity(java.lang.String city) {
		this.city = city;
	}
	/**
	 * Get country
	 */
	public java.lang.String getCountry() {
		return this.country;
	}
	
	/**
	 * Set country
	 */
	public void setCountry(java.lang.String country) {
		this.country = country;
	}
	/**
	 * Get province
	 */
	public java.lang.String getProvince() {
		return this.province;
	}
	
	/**
	 * Set province
	 */
	public void setProvince(java.lang.String province) {
		this.province = province;
	}
	
	public String toString() {
		return new StringBuffer().append("Location{")
				.append("id='").append(getId()).append("'")
				.append(",createDate='").append(getCreateDate()).append("'")
				.append(",siteId='").append(getSiteId()).append("'")
				.append(",version='").append(getVersion()).append("'")
				.append(",city='").append(getCity()).append("'")
				.append(",country='").append(getCountry()).append("'")
				.append(",province='").append(getProvince()).append("'")
				.append("}").toString();
		}
	}

