package com.koomii.myproject.rpc.service;

import java.util.List;

import com.koomii.myproject.rpc.bean.LocationBean;

public interface LocationRpcService {
	
	LocationBean findById(Integer id);
	
	/**
	 * 查询所有省份
	 * @return
	 */
	List<String> findProvinceList();
	/**
	 * 查询当前省份下的所有城市
	 * @return
	 */
	List<String> findCityList(String province);
	
	/**
	 * 跟据省 市 查询
	 * @param province
	 * @param city
	 * @return
	 */
	List<LocationBean> findLocationList(String province,String city);
}
