package com.koomii.myproject.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 风控信息DTO
 * @author david
 *
 */
public class BidRiskInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private BigDecimal creditAmount; //授信额度
	
//	private String creditExpireDate; //授信截至日期
	
	private BigDecimal creditRate; //授信利率
	
	private String borrowUse; //借款用途
	
//	private String operateSituation;  //经营情况
	
	private String securityMode; //保障方式
	
//	private String platformOpinion; //平台意见
	
	private List<String> riskPic = new ArrayList<String>(); //担保图片

	public String getBorrowUse() {
		return borrowUse;
	}

	public void setBorrowUse(String borrowUse) {
		this.borrowUse = borrowUse;
	}

	public String getSecurityMode() {
		return securityMode;
	}

	public void setSecurityMode(String securityMode) {
		this.securityMode = securityMode;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	public BigDecimal getCreditRate() {
		return creditRate;
	}

	public void setCreditRate(BigDecimal creditRate) {
		this.creditRate = creditRate;
	}

	public List<String> getRiskPic() {
		return riskPic;
	}

	public void setRiskPic(List<String> riskPic) {
		this.riskPic = riskPic;
	}
	
}
