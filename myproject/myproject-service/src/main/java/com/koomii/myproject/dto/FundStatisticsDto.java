package com.koomii.myproject.dto;

import java.io.Serializable;

/**
 * 资金统计DTO
 * @author david
 *
 */
public class FundStatisticsDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//资金统计
	private String balance="0.00"; //可用余额
	private String frozenFund="0.00"; //冻结金额
	private String totalAssets="0.00"; //总资产
	private String netAssets="0.00"; //净资产
	private String accountPayable="0.00"; //应付款
	private String accountsDue="0.00"; //应收款
	private String grossLiabilities="0.00"; //总负债
	private String accountStatus; //账户状态
	
	//投资信息
	private String totalInterest="0.00"; // 利息总额
	private String hasInvestmentIncentives="0.00"; // 已赚投资奖励
	private String hasEarlyRepayment="0.00"; // 已赚提前还款违约金
	private String weightedAverage="0.00"; // 加权平均收益率
	private String hasOverduePenalty="0.00"; // 已赚逾期罚息
	private String toBePenalty="0.00"; // 待收罚金
	private String toBeInterest="0.00"; // 待收利息
	private String totalInvestmentAccumulated="0.00"; // 累计收益
	private String hasInterest="0.00"; // 累计已收利息
	
	
//	private String totalBorrowing;// 总借款额
//	private String hasBorrowing;// 已还本息
//	private String hasAlsoBorrowing;// 已发布借款
//	private String payOffBorrowing;// 已还清借款
//	private String toBeBorrowing;// 待还本息
//	private String successBorrowing;// 成功借款
//	private String toBeSuccessBorrowing;// 未还清借款
//	// 逾期统计
//	private String beOverdueCount;// 逾期次数
//	private String beOverdueSeriousCount;// 严重逾期次数
//	private String beOverdueBorrowing;// 逾期本息
//	private String beOverdueCost;// 逾期费用

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getTotalInterest() {
		return totalInterest;
	}

	public void setTotalInterest(String totalInterest) {
		this.totalInterest = totalInterest;
	}

	public String getHasInvestmentIncentives() {
		return hasInvestmentIncentives;
	}

	public void setHasInvestmentIncentives(String hasInvestmentIncentives) {
		this.hasInvestmentIncentives = hasInvestmentIncentives;
	}

	public String getHasEarlyRepayment() {
		return hasEarlyRepayment;
	}

	public void setHasEarlyRepayment(String hasEarlyRepayment) {
		this.hasEarlyRepayment = hasEarlyRepayment;
	}

	public String getWeightedAverage() {
		return weightedAverage;
	}

	public void setWeightedAverage(String weightedAverage) {
		this.weightedAverage = weightedAverage;
	}

	public String getHasOverduePenalty() {
		return hasOverduePenalty;
	}

	public void setHasOverduePenalty(String hasOverduePenalty) {
		this.hasOverduePenalty = hasOverduePenalty;
	}

	public String getToBePenalty() {
		return toBePenalty;
	}

	public void setToBePenalty(String toBePenalty) {
		this.toBePenalty = toBePenalty;
	}

	public String getToBeInterest() {
		return toBeInterest;
	}

	public void setToBeInterest(String toBeInterest) {
		this.toBeInterest = toBeInterest;
	}

	public String getTotalInvestmentAccumulated() {
		return totalInvestmentAccumulated;
	}

	public void setTotalInvestmentAccumulated(String totalInvestmentAccumulated) {
		this.totalInvestmentAccumulated = totalInvestmentAccumulated;
	}

	public String getHasInterest() {
		return hasInterest;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getFrozenFund() {
		return frozenFund;
	}

	public void setFrozenFund(String frozenFund) {
		this.frozenFund = frozenFund;
	}

	public String getTotalAssets() {
		return totalAssets;
	}

	public void setTotalAssets(String totalAssets) {
		this.totalAssets = totalAssets;
	}

	public String getNetAssets() {
		return netAssets;
	}

	public void setNetAssets(String netAssets) {
		this.netAssets = netAssets;
	}

	public String getAccountPayable() {
		return accountPayable;
	}

	public void setAccountPayable(String accountPayable) {
		this.accountPayable = accountPayable;
	}

	public String getAccountsDue() {
		return accountsDue;
	}

	public void setAccountsDue(String accountsDue) {
		this.accountsDue = accountsDue;
	}

	public String getGrossLiabilities() {
		return grossLiabilities;
	}

	public void setGrossLiabilities(String grossLiabilities) {
		this.grossLiabilities = grossLiabilities;
	}

	public void setHasInterest(String hasInterest) {
		this.hasInterest = hasInterest;
	}
	

}
