package com.koomii.myproject.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 借款协议DTO
 * @author david
 *
 */
public class LoanProtocolDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String bidNo; //合同编号
	private String startDate; //签订时间
	private String endDate;//结束时间
	private String cycleStartDay;//每月还款开始
	
	private String realName; //借款人
	private String userName; //系统用户
	private String idCard; //身份证号码
	private String mobile; //联系电话
	private String address; //联系地址
	private String email; //电子邮箱

	private boolean isDayBid; //天标
	
	private boolean isBorrow; //是否借款人
	
	private String borrowUse;//借款详细用途
	private String borrowLimit;//借款本金数额
	private String refundWay;//还款方式
	private String monthlyPay;//月偿还本息数额
	private String lastPay; //期满偿还本息
	private int borrowTimeLimit;//还款分期月数
	private String bidUnit;//天月单位
	
	private String damages; //违约金比例
	
	private String repayManage;//借款管理费
	
	
	private String guarantor; //担保方
	private String guarantorAddress;//担保方地址
	private String guarantorMobile;//担保方电话
	//风险等级
	private String[] riskName;
	private String[] riskValue;
	//逾期罚息
	private String light;
	private String heavy;
	
	//债权人列表
	private List<LoanProtocolDto.Creditor> creditors = new ArrayList<LoanProtocolDto.Creditor>();
	//债券转让人
	private List<String> newCreditors = new ArrayList<String>();
	
	/**
	 * 初始化债权人
	 * @return
	 */
	public Creditor instanceCreditor() {
		return new Creditor();
	}
	
	/**
	 * 债权人
	 * @author david
	 *
	 */
	public class Creditor implements Serializable {
		private static final long serialVersionUID = 1L;

		private String userName; //用户名
		
		private String principal; //借出金额
		
		private String interest; //应收利息
		
		private int borrowTimeLimit; //借款期限

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public int getBorrowTimeLimit() {
			return borrowTimeLimit;
		}

		public void setBorrowTimeLimit(int borrowTimeLimit) {
			this.borrowTimeLimit = borrowTimeLimit;
		}

		public String getPrincipal() {
			return principal;
		}

		public void setPrincipal(String principal) {
			this.principal = principal;
		}

		public String getInterest() {
			return interest;
		}

		public void setInterest(String interest) {
			this.interest = interest;
		}
	}

	public String getBidNo() {
		return bidNo;
	}

	public void setBidNo(String bidNo) {
		this.bidNo = bidNo;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCycleStartDay() {
		return cycleStartDay;
	}

	public void setCycleStartDay(String cycleStartDay) {
		this.cycleStartDay = cycleStartDay;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBorrowUse() {
		return borrowUse;
	}

	public void setBorrowUse(String borrowUse) {
		this.borrowUse = borrowUse;
	}

	public String getBorrowLimit() {
		return borrowLimit;
	}

	public void setBorrowLimit(String borrowLimit) {
		this.borrowLimit = borrowLimit;
	}

	public String getRefundWay() {
		return refundWay;
	}

	public void setRefundWay(String refundWay) {
		this.refundWay = refundWay;
	}

	public String getMonthlyPay() {
		return monthlyPay;
	}

	public void setMonthlyPay(String monthlyPay) {
		this.monthlyPay = monthlyPay;
	}

	public String getLastPay() {
		return lastPay;
	}

	public void setLastPay(String lastPay) {
		this.lastPay = lastPay;
	}

	public int getBorrowTimeLimit() {
		return borrowTimeLimit;
	}

	public void setBorrowTimeLimit(int borrowTimeLimit) {
		this.borrowTimeLimit = borrowTimeLimit;
	}

	public String getBidUnit() {
		return bidUnit;
	}

	public void setBidUnit(String bidUnit) {
		this.bidUnit = bidUnit;
	}

	public String getDamages() {
		return damages;
	}

	public void setDamages(String damages) {
		this.damages = damages;
	}

	public String getRepayManage() {
		return repayManage;
	}

	public void setRepayManage(String repayManage) {
		this.repayManage = repayManage;
	}

	public String getGuarantor() {
		return guarantor;
	}

	public void setGuarantor(String guarantor) {
		this.guarantor = guarantor;
	}

	public String getGuarantorAddress() {
		return guarantorAddress;
	}

	public void setGuarantorAddress(String guarantorAddress) {
		this.guarantorAddress = guarantorAddress;
	}

	public String getGuarantorMobile() {
		return guarantorMobile;
	}

	public void setGuarantorMobile(String guarantorMobile) {
		this.guarantorMobile = guarantorMobile;
	}

	public String[] getRiskName() {
		return riskName;
	}

	public void setRiskName(String[] riskName) {
		this.riskName = riskName;
	}

	public String[] getRiskValue() {
		return riskValue;
	}

	public void setRiskValue(String[] riskValue) {
		this.riskValue = riskValue;
	}

	public String getLight() {
		return light;
	}

	public void setLight(String light) {
		this.light = light;
	}

	public String getHeavy() {
		return heavy;
	}

	public void setHeavy(String heavy) {
		this.heavy = heavy;
	}

	public List<LoanProtocolDto.Creditor> getCreditors() {
		return creditors;
	}

	public void setCreditors(List<LoanProtocolDto.Creditor> creditors) {
		this.creditors = creditors;
	}

	public List<String> getNewCreditors() {
		return newCreditors;
	}

	public void setNewCreditors(List<String> newCreditors) {
		this.newCreditors = newCreditors;
	}

	public boolean isDayBid() {
		return isDayBid;
	}

	public void setDayBid(boolean isDayBid) {
		this.isDayBid = isDayBid;
	}

	public boolean isBorrow() {
		return isBorrow;
	}

	public void setBorrow(boolean isBorrow) {
		this.isBorrow = isBorrow;
	}

}
