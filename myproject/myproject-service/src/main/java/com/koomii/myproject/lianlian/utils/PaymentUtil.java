package com.koomii.myproject.lianlian.utils;

import java.io.UnsupportedEncodingException;

import org.apache.commons.lang.StringUtils;

import net.sf.json.JSONObject;

import com.koomii.myproject.lianlian.vo.PaymentInfo;
import com.koomii.myproject.lianlian.vo.PaymentNotify;
import com.koomii.myproject.lianlian.vo.PaymentReturn;

/**
 * 连连支付工具类
 * @author david
 *
 */
public class PaymentUtil {

	/**
	 * 转换回显数据
	 * @param json
	 * @return
	 */
	public static PaymentReturn convertPaymentReturn(String json) {
		PaymentReturn o = null;
		if(StringUtils.isNotBlank(json)) {
			o = new PaymentReturn();
			
			JSONObject v = JSONObject.fromObject(json);
			String oid_partner = getStringValue(v,"oid_partner"); //商户编号
			String dt_order = getStringValue(v,"dt_order"); //商户订单时间
			String no_order = getStringValue(v,"no_order"); //商户唯一订单号
			String oid_paybill = getStringValue(v,"oid_paybill"); //连连支付支付订单号
			String money_order = getStringValue(v,"money_order"); //交易金额
			String result_pay = getStringValue(v,"result_pay"); //支付结果
			String settle_date = getStringValue(v,"settle_date"); //清算日期
			String sign_type = getStringValue(v,"sign_type"); //签名方式
			String sign = getStringValue(v,"sign"); //签名
			
			o.setOid_partner(oid_partner);
			o.setDt_order(dt_order);
			o.setNo_order(no_order);
			o.setOid_paybill(oid_paybill);
			o.setMoney_order(money_order);
			o.setResult_pay(result_pay);
			o.setSettle_date(settle_date);
			o.setSign_type(sign_type);
			o.setSign(sign);
		}
		return o;
	}
	
	/**
	 * 转换支付响应数据
	 * @param json
	 * @return
	 */
	public static PaymentNotify convertPaymentNotify(String json) {
		PaymentNotify o = null;
		if(StringUtils.isNotBlank(json)) {
			o = new PaymentNotify();
			
			JSONObject v = JSONObject.fromObject(json);
			
			String oid_partner = getStringValue(v,"oid_partner"); //商户编号
			String dt_order = getStringValue(v,"dt_order"); //商户订单时间
			String no_order = getStringValue(v,"no_order"); //商户唯一订单号
			String oid_paybill = getStringValue(v,"oid_paybill"); //连连支付支付订单号
			String money_order = getStringValue(v,"money_order"); //交易金额
			String result_pay = getStringValue(v,"result_pay"); //支付结果
			String settle_date = getStringValue(v,"settle_date"); //清算日期
			String info_order = getStringValue(v,"info_order"); //订单描述
			String pay_type = getStringValue(v,"pay_type"); //支付方式
			String bank_code = getStringValue(v,"bank_code"); //银行编号
			String no_agree = getStringValue(v,"no_agree"); //签约协议号
			String id_type = getStringValue(v,"id_type"); //证件类型 默认为 0 身份证
			String id_no = getStringValue(v,"id_no"); //证件号码
			String acct_name = getStringValue(v,"acct_name"); //银行账号姓名
			String card_no = getStringValue(v,"card_no"); //银行卡号  显示前 6后 4 4 隐
			String sign_type = getStringValue(v,"sign_type"); //签名方式
			String sign = getStringValue(v,"sign"); //签名
			
			o.setOid_partner(oid_partner);
			o.setDt_order(dt_order);
			o.setNo_order(no_order);
			o.setOid_paybill(oid_paybill);
			o.setMoney_order(money_order);
			o.setResult_pay(result_pay);
			o.setSettle_date(settle_date);
			o.setInfo_order(info_order);
			o.setPay_type(pay_type);
			o.setBank_code(bank_code);
			o.setNo_agree(no_agree);
			o.setId_type(id_type);
			o.setId_no(id_no);
			o.setAcct_name(acct_name);
			o.setCard_no(card_no);
			o.setSign_type(sign_type);
			o.setSign(sign);
		}
		return o;
	}
	
	private static String getStringValue(JSONObject o,String key) {
		String value = null;
		if(o.containsKey(key)) {
			value = o.getString(key);
		}
		return value;
	}
	
	/**
	 * 生成密钥签名
	 * @param info 支付实体
	 * @param signType 签名类型 MD5 RSA
	 * @param signKey 密钥
	 * @return
	 */
	public static String generateSign(PaymentInfo info,String signType,String signKey) {
		String sign = "";
		if(info!=null) {
			StringBuffer sb = new StringBuffer("");
			if(StringUtils.isNotBlank(info.getAcct_name())) {
				sb.append("&acct_name=");sb.append(info.getAcct_name());
			}
			if(StringUtils.isNotBlank(info.getApp_request())) {
				sb.append("&app_request=");sb.append(info.getApp_request());
			}
			if(StringUtils.isNotBlank(info.getBusi_partner())) {
				sb.append("&busi_partner=");sb.append(info.getBusi_partner());
			}
			if(StringUtils.isNotBlank(info.getCard_no())) {
				sb.append("&card_no=");sb.append(info.getCard_no());
			}
			if(StringUtils.isNotBlank(info.getDt_order())) {
				sb.append("&dt_order=");sb.append(info.getDt_order());
			}
			if(StringUtils.isNotBlank(info.getId_no())) {
				sb.append("&id_no=");sb.append(info.getId_no());
			}
			if(StringUtils.isNotBlank(info.getId_type())) {
				sb.append("&id_type=");sb.append(info.getId_type());
			}
			if(StringUtils.isNotBlank(info.getInfo_order())) {
				sb.append("&info_order=");sb.append(info.getInfo_order());
			}
			if(StringUtils.isNotBlank(info.getMoney_order())) {
				sb.append("&money_order=");sb.append(info.getMoney_order());
			}
			if(StringUtils.isNotBlank(info.getName_goods())) {
				sb.append("&name_goods=");sb.append(info.getName_goods());
			}
			if(StringUtils.isNotBlank(info.getNo_agree())) {
				sb.append("&no_agree=");sb.append(info.getNo_agree());
			}
			if(StringUtils.isNotBlank(info.getNo_order())) {
				sb.append("&no_order=");sb.append(info.getNo_order());
			}
			if(StringUtils.isNotBlank(info.getNotify_url())) {
				sb.append("&notify_url=");sb.append(info.getNotify_url());
			}
			if(StringUtils.isNotBlank(info.getOid_partner())) {
				sb.append("&oid_partner=");sb.append(info.getOid_partner());
			}
			if(StringUtils.isNotBlank(info.getPlatform())) {
				sb.append("&platform=");sb.append(info.getPlatform());
			}
			if(StringUtils.isNotBlank(info.getRisk_item())) {
				sb.append("&risk_item=");sb.append(info.getRisk_item());
			}
			if(StringUtils.isNotBlank(info.getSign_type())) {
				sb.append("&sign_type=");sb.append(info.getSign_type());
			}
			if(StringUtils.isNotBlank(info.getUrl_return())) {
				sb.append("&url_return=");sb.append(info.getUrl_return());
			}
			if(StringUtils.isNotBlank(info.getUser_id())) {
				sb.append("&user_id=");sb.append(info.getUser_id());
			}
			if(StringUtils.isNotBlank(info.getValid_order())) {
				sb.append("&valid_order=");sb.append(info.getValid_order());
			}
			if(StringUtils.isNotBlank(info.getVersion())) {
				sb.append("&version=");sb.append(info.getVersion());
			}
			
			String sign_src = sb.substring(1);
			if("RSA".equals(signType)) {
				sign = RSAUtil.sign(signKey, sign_src);
			} else {
				sign_src += "&key=" + signKey;
		        try {
					sign = Md5Algorithm.getInstance().md5Digest(sign_src.getBytes("utf-8"));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		}
		return sign;
	}
}
