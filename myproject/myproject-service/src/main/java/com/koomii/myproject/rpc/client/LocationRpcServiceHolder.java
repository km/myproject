package com.koomii.myproject.rpc.client;

import java.util.List;

import com.koomii.myproject.rpc.bean.LocationBean;
import com.koomii.myproject.rpc.service.LocationRpcService;

public class LocationRpcServiceHolder {
	private LocationRpcService locationRpcService;
	
	public LocationBean findById(Integer id){
		return locationRpcService.findById(id);
	}
	
	/**
	 * 查询所有省份
	 * @return
	 */
	public List<String> findProvinceList(){
		return locationRpcService.findProvinceList();
	}
	/**
	 * 查询当前省份下的所有城市
	 * @return
	 */
	public List<String> findCityList(String province){
		return locationRpcService.findCityList(province);
	}
	
	/**
	 * 跟据省 市 查询
	 * @param province
	 * @param city
	 * @return
	 */
	public List<LocationBean> findLocationList(String province,String city){
		return locationRpcService.findLocationList(province, city);
	}

	public void setLocationRpcService(LocationRpcService locationRpcService) {
		this.locationRpcService = locationRpcService;
	}
	
}
