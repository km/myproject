package com.koomii.myproject.rpc.server;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;

import com.koomii.myproject.domain.entity.Location;
import com.koomii.myproject.rpc.bean.LocationBean;
import com.koomii.myproject.rpc.service.LocationRpcService;
import com.koomii.myproject.service.LocationService;

public class LocationRpcServiceImpl implements LocationRpcService {
	
	private LocationService locationService;
	
	@Override
	public LocationBean findById(Integer id) {
		Location location = locationService.findById(id);
		if(location == null){
			return null;
		}
		LocationBean lb = new LocationBean();
		BeanUtils.copyProperties(location, lb);
		return lb;
	}

	@Override
	public List<String> findProvinceList() {
		return locationService.findProvinceList();
	}

	@Override
	public List<String> findCityList(String province) {
		return locationService.findCityList(province);
	}

	@Override
	public List<LocationBean> findLocationList(String province, String city) {
		List<Location> locationList = locationService.findLocationList(province, city);
		if(CollectionUtils.isEmpty(locationList)){
			return null;
		}
		List<LocationBean> resultList = new ArrayList<LocationBean>();
		for(Location location : locationList){
			LocationBean lb = new LocationBean();
			BeanUtils.copyProperties(location, lb);
			resultList.add(lb);
		}
		return resultList;
	}

	
	public void setLocationService(LocationService locationService) {
		this.locationService = locationService;
	}
	
}
