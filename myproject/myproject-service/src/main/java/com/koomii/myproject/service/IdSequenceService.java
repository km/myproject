package com.koomii.myproject.service;



/**
 *
 * IdSequenceService Id生成器服务
 *
 * @author 
 *
 */
public interface IdSequenceService{

	/**
	 * 获取Id
	 * @param idKey
	 * @return
	 */
	Integer getId(String idKey,Integer stepValue);
}