package com.koomii.myproject.service;


import java.util.List;

import com.koomii.myproject.domain.entity.Location;

/**
 *
 * LocationService
 *
 * @author 
 *
 */
public interface LocationService{
	
	Long save(Location location);

	void update(Location location);

	void delete(Long id);

	Location findById(Integer id);
	
	/**
	 * 查询所有省份
	 * @return
	 */
	List<String> findProvinceList();
	/**
	 * 查询当前省份下的所有城市
	 * @return
	 */
	List<String> findCityList(String province);
	
	/**
	 * 跟据省 市 查询
	 * @param province
	 * @param city
	 * @return
	 */
	List<Location> findLocationList(String province,String city);
}