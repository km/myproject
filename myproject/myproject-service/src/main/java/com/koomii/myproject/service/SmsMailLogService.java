package com.koomii.myproject.service;

import com.koomii.myproject.common.MessageBean;
import com.koomii.myproject.domain.entity.SmsMailLog;

/**
 *
 * SmsMailLogService
 *
 * @author 
 *
 */
public interface SmsMailLogService{
	
	Integer save(SmsMailLog smsMailLog);

	void update(SmsMailLog smsMailLog);

	void delete(Integer id);

	SmsMailLog findById(Integer id);
	
	/**
	 * 验证手机验证码
	 * @param mobile 手机号
	 * @param verifycode 验证码
	 * @return
	 */
	MessageBean validateMobileCode(String mobile,String verifycode);
}