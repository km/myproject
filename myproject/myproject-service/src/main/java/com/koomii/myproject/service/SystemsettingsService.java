package com.koomii.myproject.service;


import com.koomii.myproject.domain.entity.Systemsettings;

/**
 *
 * SystemsettingsService
 *
 * @author 
 *
 */
public interface SystemsettingsService{
	
	Integer save(Systemsettings systemsettings);

	void update(Systemsettings systemsettings);

	void delete(Integer id);

	Systemsettings findById(Integer id);
	
	/**
	 * 查询配置
	 * @param paramName
	 * @param siteId
	 * @return
	 */
	Systemsettings findByParam(String paramName,long siteId);
}