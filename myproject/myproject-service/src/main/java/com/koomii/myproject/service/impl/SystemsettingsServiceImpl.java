package com.koomii.myproject.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.koomii.myproject.service.SystemsettingsService;
import com.koomii.myproject.dao.SystemsettingsDao;
import com.koomii.myproject.domain.entity.Systemsettings;

/**
 *
 * SystemsettingsServiceImpl
 *
 * @author 
 *
 */
@Service("systemsettingsService")
public class SystemsettingsServiceImpl  implements SystemsettingsService{
	
	@Autowired
	private SystemsettingsDao systemsettingsDao;
	
	@Override
	public Integer save(Systemsettings systemsettings){
		return systemsettingsDao.save(systemsettings);
	}

	@Override
	public void update(Systemsettings systemsettings){
		systemsettingsDao.update(systemsettings);
	}

	@Override
	public void delete(Integer id){
		systemsettingsDao.delete(id);
	}

	@Override
	public Systemsettings findById(Integer id){
		return systemsettingsDao.findById(id);
	}

	@Override
	public Systemsettings findByParam(String paramName, long siteId) {
		List<Systemsettings> list = systemsettingsDao.findByParam(paramName,siteId);
		if(list!=null && list.size()>0) {
			return list.get(0);
		} else {
			return null;
		}
	}
	
}