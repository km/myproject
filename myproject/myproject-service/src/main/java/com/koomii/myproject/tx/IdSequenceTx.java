package com.koomii.myproject.tx;

/**
 * Id主键生成事务
 * @author lizongyue
 *
 */
public interface IdSequenceTx {
	
	/**
	 * Id生成事务
	 * 如果没有查询到，则创建新的
	 * @return
	 */
	Integer getAndUpdateNextIdSequence(final String idKey,final Integer firstValue);
}
