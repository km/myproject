package com.koomii.myproject.web.controller;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.koomii.common.util.StringUtils;


/**
 * Created with IntelliJ IDEA.
 * User: Greey
 * Date: 14-12-4
 * Time: 上午11:39
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseController {
	
	protected HttpServletRequest getHttpServletRequest() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		return request;
	}
	
	protected Integer getIntegerValue(String value){
		return getIntegerValue(value, 0);
	}
	
	protected Integer getIntegerValue(String value,Integer defaultValue){
		try {
			return Integer.parseInt(value);
		} catch (Exception e) {
			return defaultValue;
		}
	}
	
	protected String getStringParameter(String name) {
		return getStringParameter(name, null);
	}

	protected String getStringParameter(String name, String defaultValue) {
		String value = getHttpServletRequest().getParameter(name);

		return StringUtils.isEmpty(value) ? defaultValue : value.trim();
	}

	protected Boolean getBooleanParameter(String name) {
		return getBooleanParameter(name, null);
	}

	protected Boolean getBooleanParameter(String name, Boolean defaultValue) {
		String value = getHttpServletRequest().getParameter(name);

		return StringUtils.isEmpty(value) ? defaultValue : Boolean.valueOf(value);
	}

	protected Integer getIntegerParameter(String name) {
		return getIntegerParameter(name, null);
	}
	
	protected Integer getIntegerParameter(String name, Integer defaultValue) {
		String value = getHttpServletRequest().getParameter(name);
		
		return StringUtils.isEmpty(value) ? defaultValue : new Integer(value);
	}

	protected Long getLongParameter(String name) {
		return getLongParameter(name, null);
	}

	protected Long getLongParameter(String name, Long defaultValue) {
		String value = getHttpServletRequest().getParameter(name);

		return StringUtils.isEmpty(value) ? defaultValue : Long.valueOf(value);
	}

	protected Float getFloatParameter(String name) {
		return getFloatParameter(name, null);
	}

	protected Float getFloatParameter(String name, Float defaultValue) {
		String value = getHttpServletRequest().getParameter(name);

		return StringUtils.isEmpty(value) ? defaultValue : Float.valueOf(value);
	}

	protected Double getDoubleParameter(String name) {
		return getDoubleParameter(name, null);
	}

	protected Double getDoubleParameter(String name, Double defaultValue) {
		String value = getHttpServletRequest().getParameter(name);

		return StringUtils.isEmpty(value) ? defaultValue : Double.valueOf(value);
	}

	protected Date getDateParameter(String name, String format) {
		return getDateParameter(name, format, null);
	}

	protected Date getDateParameter(String name, String format, String defaultValue) {
		String value = getHttpServletRequest().getParameter(name);
		DateFormat dateFormat = new SimpleDateFormat(format);

		try {
			if (StringUtils.isEmpty(value)) {
				return null == defaultValue ? null : dateFormat.parse(defaultValue);
			} else {
				return dateFormat.parse(value);
			}
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * ajax中文乱码转换，注意仅用于jquery ajax
	 * @return
	 */
	public static String chineseDecode(String str){
		if(str == null){
			return str;
		}
		try {
			str = new String(str.getBytes("ISO-8859-1"),"UTF-8");
		} catch (UnsupportedEncodingException e1) {
		}
		return str;
	}
}
