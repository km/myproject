package com.koomii.myproject.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.koomii.myproject.common.MessageBean;
import com.koomii.myproject.config.GlobalConfig;
import com.koomii.myproject.domain.entity.Location;
import com.koomii.myproject.domain.entity.Systemsettings;
import com.koomii.myproject.service.LocationService;
import com.koomii.myproject.service.SmsMailLogService;
import com.koomii.myproject.service.SystemsettingsService;
import com.koomii.myproject.util.RegexUtils;
import com.koomii.common.cached.CacheUtil;

/**
 * 公用服务
 * @author david
 *
 */
@Controller
@RequestMapping(value = "/common")
public class CommonController extends BaseController {
	private static final Log log = LogFactory.getLog(CommonController.class);
	@Value("${isSendMobile}")
	private String isSendMobile;
	@Autowired
	private LocationService locationService;
	@Autowired
	private SystemsettingsService systemsettingsService;
	@Autowired
	private SmsMailLogService smsMailLogService;
//	@Autowired
//	private CacheUtil cacheUtil;
	/**
	 * 省份列表
	 * @return
	 */
	@RequestMapping(value = "/provincelist")
	@ResponseBody
	public List<String> provincelist() {
		List<String> list = locationService.findProvinceList();
		return list;
	}
	
	/**
	 * 市列表
	 * @param province 省
	 * @return
	 */
	@RequestMapping(value = "/citylist")
	@ResponseBody
	public List<String> citylist(@RequestParam(required = true)String province) {
		if(StringUtils.isEmpty(province)){
			return new ArrayList<String>();
		}
		province = chineseDecode(province);
		List<String> list = locationService.findCityList(province);
		return list;
	}
	
	
	/**
	 * 县列表
	 * @param province 省
	 * @param city 市
	 * @return
	 */
	@RequestMapping(value = "/countylist/{province}/{city}")
	@ResponseBody
	public List<Location> countylist(@PathVariable String province,@PathVariable String city) {
		//PathVariable的方式中文不用转码，但是?传参的需要转
		if(StringUtils.isEmpty(province) || StringUtils.isEmpty(city)){
			return new ArrayList<Location>();
		}
		List<Location> list = locationService.findLocationList(province, city);
		return list;
	}
    
}
