package com.koomii.myproject.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.koomii.common.util.PaginatedList;
import com.koomii.common.web.LoginContext;

/**
 * User: lzy
 */
@Controller
public class IndexController extends BaseController {
	
    
    @RequestMapping(value = "/")
    public String index(Model model) {
    	 LoginContext loginContext =  LoginContext.getLoginContext();
         if(loginContext==null || !loginContext.isLogin()){
         	return "redirect:/login?time="+System.currentTimeMillis();
         } else {
        	 return "redirect:/home";
         }
    }
    
    @RequestMapping(value = "/login")
    public String login(Model model,HttpServletRequest request,HttpServletResponse response,String time) {
    	model.addAttribute("time", time==null?"time:null" : "time:"+time);
    	return "login";
    }
    
    @RequestMapping(value = "/index/login")
    public String indexLogin(Model model,HttpServletRequest request,HttpServletResponse response,String time) {
    	model.addAttribute("time", time==null?"time:null" : "time:"+time);
    	return "login";
    }
    
    @RequestMapping(value = "/home")
    public String home(Model model) {
    	return "index";
    }
}
