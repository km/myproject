package com.koomii.myproject.web.interceptor;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.koomii.common.util.JsonUtil;
import com.koomii.common.web.security.WebSecurity;

/**
 * API接口鉴权
 * @author david
 *
 */
public class ApiInterceptor extends HandlerInterceptorAdapter {
	
	private static final String TOKEN = "token";
	
	//URL权限配置
	protected WebSecurity apiSecurity;
//	@Autowired
//	private UserTokenService tokenService;
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		String path = request.getRequestURI();
//		MessageBean msg = new MessageBean();
		/*if (apiSecurity != null) {
            //检查是否在受保护范围
            if (apiSecurity.isProtected(path)) {
            	String token = (String) request.getAttribute(TOKEN);
				if (StringUtils.isEmpty(token)) {
					token = (String) request.getParameter(TOKEN);
				}
				//如果token为空
				if(StringUtils.isBlank(token)) {
					 msg.setApiCode(BizCode.LOGIN_TIMEOUT);
					 msg.fireFailed();
					 writeJson(msg, response);
					 return false;
				}
				UserToken tokenBean = tokenService.findByToken(token);
				if(tokenBean == null) {
					 msg.setApiCode(BizCode.LOGIN_TIMEOUT);
					 msg.fireFailed();
					 writeJson(msg, response);
					 return false;
				}
				
				Date acitvetime = tokenBean.getActiveTime(); //活动时间
				Date now = new Date(); //当前时间
				Date expilyDate = DateUtils.addDays(acitvetime, 30); //过期时间
				Date updateActive = DateUtils.addDays(acitvetime, 25); //更新Token时间
				//过期
				if(now.getTime()>=expilyDate.getTime()) {
					//删除token
					tokenService.delete(tokenBean.getId());
					
		            msg.setApiCode(BizCode.LOGIN_TIMEOUT);
				    msg.fireFailed();
		            
		            writeJson(msg, response);
		            return false;
				}
				//如果当前时间大于超时时间前5天，刷新活动时间
				if(now.getTime()>updateActive.getTime()) {
					tokenBean.setActiveTime(now);
					tokenService.update(tokenBean);
				}
            }
        } else {
            writeJson(msg, response);
            return false;
        }*/
		return super.preHandle(request, response, handler);
	}
	
	/**
	 * 写入相应对象
	 * @param msg
	 * @param response
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	/*private void writeJson(MessageBean msg,HttpServletResponse response) throws MalformedURLException, IOException {
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json");
		response.getWriter().write(JsonUtil.g.toJson(msg));
	}*/
	
	public WebSecurity getApiSecurity() {
		return apiSecurity;
	}

	public void setApiSecurity(WebSecurity apiSecurity) {
		this.apiSecurity = apiSecurity;
	}
}
