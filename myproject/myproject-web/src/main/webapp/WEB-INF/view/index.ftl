<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>${(systemConfig.shopName)!}-${systemConfig.metaKeywords}</title>
<meta name="Author" content="Migrate Mobile Cloud Business Software Company" />
<meta name="Copyright" content="Migrate Mobile Cloud Business Software Company" />
<#if (systemConfig.metaKeywords)! != ""><meta name="keywords" content="${systemConfig.metaKeywords}" /></#if>
<#if (systemConfig.metaDescription)! != ""><meta name="description" content="${systemConfig.metaDescription}" /></#if>
</head>
<#include "common/include.ftl">
<body>
    <!--顶部信息栏目 开始--->
    <#include "common/top.ftl">
    <!--顶部信息栏目 结束--->
	<div style="display:block;height:300px;width:800px;padding-top: 100px;">
		首页
	</div>
	<!--底部信息栏目 开始--->
    <#include "common/footer.ftl">
    <!--底部信息栏目 结束--->
</div>
</div>
</body>
</html>
