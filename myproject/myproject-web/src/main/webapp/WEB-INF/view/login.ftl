<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>${(systemConfig.shopName)!}-${systemConfig.metaKeywords}</title>
<meta name="Author" content="Migrate Mobile Cloud Business Software Company" />
<meta name="Copyright" content="Migrate Mobile Cloud Business Software Company" />
<#if (systemConfig.metaKeywords)! != ""><meta name="keywords" content="${systemConfig.metaKeywords}" /></#if>
<#if (systemConfig.metaDescription)! != ""><meta name="description" content="${systemConfig.metaDescription}" /></#if>
</head>
<#include "common/include.ftl">
<body>
    <!--顶部信息栏目 开始--->
    <#include "common/top.ftl">
    <!--顶部信息栏目 结束--->
	<div style="display:block;height:300px;width:800px;padding-top: 100px;">
		登录页<br>
		${time}<br><br><br><br>
		<a href="home" target="_blank">首页Freemakert页面</a><br><br>
		<a href="index/login?time=123" target="_blank">子登录页，测试&lt;base&gt;标签</a><br><br>
		<a href="common/provincelist" target="_blank">省份获取,json</a><br><br>
		<#assign province="河北省"/>
		<#assign city="石家庄市"/>
		<#setting url_escaping_charset='utf-8'>
		<a href="common/citylist?province=${province?url}" target="_blank">城市获取,json带请求参数</a><br><br>
		<a href="common/countylist/${province?url}/${city?url}" target="_blank">城市获取,json带请求参数Restful风格</a><br><br>
	</div>
	<!--底部信息栏目 开始--->
    <#include "common/footer.ftl">
    <!--底部信息栏目 结束--->
</div>
</div>
</body>
</html>
