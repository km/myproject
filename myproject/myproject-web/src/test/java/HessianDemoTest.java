import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.koomii.myproject.rpc.client.LocationRpcServiceHolder;

import static org.junit.Assert.*;

@ContextConfiguration(locations={"classpath:spring/spring.xml","classpath:spring/spring-hessian-client.xml"})
public class HessianDemoTest extends AbstractJUnit4SpringContextTests {
	
	@Autowired
	private LocationRpcServiceHolder localtionRpcServiceHolder;
	
	@Test
	public void findProvinceListTest() {
		try {
			List<String> ls = localtionRpcServiceHolder.findProvinceList();
			System.out.println(ls);
			assertTrue(ls.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
